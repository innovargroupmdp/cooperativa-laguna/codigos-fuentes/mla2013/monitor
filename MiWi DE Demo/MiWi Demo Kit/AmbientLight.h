/* 
 * File:   Ambientlight.h
 * Author: cba
 *
 * Created on December 21, 2023, 12:36 PM
 */

#ifndef AMBIENTLIGHTSENSOR_H
#define	AMBIENTLIGHTSENSOR_H

#include "Sensor.h"

struct Ambientlight
{ 
  /*Standar API */
  Sensor sensor;
  //Main Measures
  unsigned long  luminance;
  /*MIMIA extention*/
  unsigned long  past;
  const Tech *type_t;
  unsigned int id;  
};

typedef struct Ambientlight Ambientlight;


/********************************************************************
 * Instance properties
 ********************************************************************/
BOOL activated(void);
BOOL hasReading(void);
void timestamp(void);
/********************************************************************
 * Instance methods
 ********************************************************************/
void start(void);
void stop(void);
//constructor
void ambientLigth_init(Ambientlight*, Sense*, unsigned int, Tech* );
/********************************************************************
 * Events
 ********************************************************************/
void onActivate(void);
void onError(void);
void onReading(void);

#endif	/* AMBIENTLIGHTSENSOR_H */

