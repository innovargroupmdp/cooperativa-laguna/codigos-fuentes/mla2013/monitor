/* 
 * File:   test.h
 * Author: cba
 *
 * Created on November 11, 2020, 4:28 PM
 */

#include "GenericTypeDefs.h"

struct Data_Test_Def_t{
    char Tipo[5];
    UINT16 Data;
};

struct Data_base_Test_Def_t{
    
    struct   Data_Test_Def_t msg[20];
    UINT16 index;
    
};

typedef struct Data_base_Test_Def_t Data_base_Test_Def;

void clear_items(void);
void Add_item( char*, UINT16 );
void Print_items(void);