/*******************************************************************************
Copyright 2016 Microchip Technology Inc. (www.microchip.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*******************************************************************************/
#include <p18lf26j50.h>
#include "buttons.h"
#include "../BSP_MiWiDemoBd.h"
/*** Button Definitions *********************************************/
//      S1  is MCLR button


#define BUTTON_PRESSED      0
#define BUTTON_NOT_PRESSED  1

#define PIN_INPUT           1
#define PIN_OUTPUT          0

/*********************************************************************
 * Function: BOOL BUTTON_IsPressed(BUTTON button);
 *
 * Overview: Returns the current state of the requested button
 *
 * PreCondition: button configured via BUTTON_SetConfiguration()
 *
 * Input: BUTTON button - enumeration of the buttons available in
 *        this demo.  They should be meaningful names and not the names
 *        of the buttons on the silk-screen on the board (as the demo
 *        code may be ported to other boards).
 *         i.e. - ButtonIsPressed(BUTTON_SEND_MESSAGE);
 *
 * Output: TRUE if pressed; FALSE if not pressed.
 *
 ********************************************************************/
BOOL BUTTON_IsPressed ( BUTTON button )
{
    switch(button)
    {
#ifdef SW1_PORT

        case BUTTON_S3:
            return ( (SW1_PORT == BUTTON_PRESSED)? TRUE : FALSE);
#endif

#ifdef SW2_PORT
        case BUTTON_S4:
            return ( ( SW2_PORT == BUTTON_PRESSED )? TRUE : FALSE ) ;
#endif

        default:
            return FALSE;
    }
    
    return FALSE;
}

/*********************************************************************
* Function: void BUTTON_Enable(BUTTON button);
*
* Overview: Returns the current state of the requested button
*
* PreCondition: button configured via BUTTON_SetConfiguration()
*
* Input: BUTTON button - enumeration of the buttons available in
*        this demo.  They should be meaningful names and not the names
*        of the buttons on the silk-screen on the board (as the demo
*        code may be ported to other boards).
*         i.e. - ButtonIsPressed(BUTTON_SEND_MESSAGE);
*
* Output: None
*
********************************************************************/
void BUTTON_Enable(BUTTON button)
{
    switch(button)
    {
#ifdef SW1_TRIS
        case BUTTON_S3:
            SW1_TRIS = PIN_INPUT ;
            break ;
#endif      
            
#ifdef SW2_TRIS

        case BUTTON_S4:
            SW2_TRIS = PIN_INPUT ;
            break ;
#endif      

        default:
            break ;
    }
}
