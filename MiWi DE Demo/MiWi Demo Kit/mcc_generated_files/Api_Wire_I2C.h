/* 
 * File:   Api_Wire_I2C.h
 * Author: cba
 *
 * Created on November 3, 2020, 1:34 PM
 */

#ifndef API_WIRE_I2C_H
#define	API_WIRE_I2C_H

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* API_WIRE_I2C_H */

#define SLAVE_I2C_GENERIC_DEVICE_TIMEOUT    50  // define slave timeout 
#define SLAVE_I2C_GENERIC_RETRY_MAX         100


BOOL Wire_Put( UINT8 *, UINT16 , UINT8 );

BOOL Wire_Put_R_S(UINT16 , UINT8 *,UINT16, UINT8 );

BOOL Wire_Get(UINT8 *, UINT16 ,UINT8 );
