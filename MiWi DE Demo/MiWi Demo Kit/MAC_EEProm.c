/********************************************************************
/********************************************************************
* FileName:		MAC_EEprom.c
* Dependencies:    
* Processor:	PIC18	
* Complier:     Microchip C18 v3.04 or higher
* Company:		Microchip Technology, Inc.
*
* Copyright and Disclaimer Notice
*
* Copyright � 2007-2010 Microchip Technology Inc.  All rights reserved.
*
* Microchip licenses to you the right to use, modify, copy and distribute 
* Software only when embedded on a Microchip microcontroller or digital 
* signal controller and used with a Microchip radio frequency transceiver, 
* which are integrated into your product or third party product (pursuant 
* to the terms in the accompanying license agreement).   
*
* You should refer to the license agreement accompanying this Software for 
* additional information regarding your rights and obligations.
*
* SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY 
* KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY 
* WARRANTY OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A 
* PARTICULAR PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE 
* LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, 
* CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY 
* DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO 
* ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, 
* LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, 
* TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT 
* NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*
*********************************************************************
* File Description:
*
*  This file reads the 25AA02E48 MAC EEProm
*
* Change History:
*  Rev   Date         Author    Description
*  1.0   2/02/2011    ccs       Initial revision
********************************************************************/
#include "MAC_EEProm.h"
#include "SystemProfile.h"
#include "WirelessProtocols/MSPI.h"


#define SPI_WRT_STATUS  0x01
#define SPI_WRITE       0x02
#define SPI_READ        0x03
#define SPI_DIS_WRT     0x04
#define SPI_RD_STATUS   0x05
#define SPI_EN_WRT      0x06

#define EEPROM_MAC_ADDR 0xFA

extern BYTE myLongAddress[]; //definida en MiWI.c

/*********************************************************************
* Function:         void Read_MAC_Address(void)
*
* PreCondition:     none
*
* Input:		    none
*
* Output:		    none
*
* Side Effects:	    none
*
* Overview:		    Following routine reads the MAC Address form the
*                   EEProm and loads it into myLongAddress[].
*                    
*
* Note:			    
**********************************************************************/  
void Read_MAC_Address(void)
{
    
    // Microchip OUI (0x 00 04 A3 tambien 04d3
	
    myLongAddress[0]=0x01;
    myLongAddress[1]=0x00;
    myLongAddress[2]=0x00;
    myLongAddress[3]=0x00;
    myLongAddress[4]=0x00;
    myLongAddress[5]=0xA3;
    myLongAddress[6]=0x04;
    myLongAddress[7]=0x00;
}



    	