/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include "mcc_generated_files/interrupt_manager.h"
/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/

// Designate Sample_ISR as an interrupt function and save key registers

//#pragma interruptlow Low_ISR //save = PRODL,PRODH,temp
// Locate ISR handler code at interrupt vector
/*
#pragma code isrcode_low= 0x0018
void Vecto_isrhandler_low(void)	// This function directs execution to the
{								// actual interrupt code										
_asm
    goto Low_ISR  
_endasm
}
#pragma code
*/
/* High-priority service */
//#pragma interrupt High_ISR //save = PRODL,PRODH,temp
// Locate ISR handler code at interrupt vector
/*#pragma code isrcode_high= 0x0008
void Vector_isrhandler_high(void)	// This function directs execution to the
{								// actual interrupt code										
_asm
    goto High_ISR  
_endasm
}
#pragma code
*/
      