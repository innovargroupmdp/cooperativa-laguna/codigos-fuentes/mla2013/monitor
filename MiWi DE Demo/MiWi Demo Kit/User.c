#include <p18lf26j50.h>
#include <BSP_MiWiDemoBd.h>
#include <stdlib.h>
#include "TimeDelay.h"
#include "User.h"
#include "Sensor.h"
#include "App.h"
#include "APDS/Counter.h"

#pragma udata
    WORD Counter;                                                                   // alternativa para medicion
#pragma udata
    
void FunChekOSC(void){
  if (PIR2bits.OSCFIF){
      FunAwake();
      ClrWdt();        
  }
}

void StckChck(void)
{
    //if ((STKPTR & 0x1f)>27)  LED3=1;

    return;
}

void FunClearGlobal(void){
    
    BYTE i;
    
    App.ID=0;
    App.T=0;
    App.Sec=0;
    App.N=0;
    App.Noise=0;
    App.Singnal=0;
    App.Bat=0;
    App.Slot=0;
    App.DateTx.Seg=0;
    App.DateTx.Min=0;
    App.DateTx.Hou=0;
    App.DateTx.Day=0;
    App.DateTx.Mon=0;
    App.DateTx.Yea=0;
    App.MedidaRE.DateMesu.Hou=0;
    App.MedidaRE.DateMesu.Day=0;
    App.MedidaRE.DateMesu.Mon=0;
    App.MedidaRE.Mesure=0;
    App.MedidaDif.Index=0;
    
    for (i=0;i< LengMesure;i++ )
        App.MedidaDif.MedidaD[i]=0;
        
    App.SlotDif=0;
            
    return;
}


/* solo habilito la parte de los timers relacionados RTCC
 */
void InitInts(void){   
  static BOOL f=0;
  BYTE Intcon;

  /*solo estan una sola vez*/
  if (f) return;
  f= TRUE;

  //seteo a cero el RTCC 
  Fun_Rtcc_Fix();
  //Desactiva INT TMR0 despierta de modo incorrecto al MEDIdor !!!!
  Intcon= INTCON;                                                             //timer de ieee para randon y SEC
  INTCON=0;
  //INTCONbits.PEIE= 0;
  //INTCONbits.GIE= 0;
  PIE3bits.RTCCIE= 0;
  /*****************************************************************************
   * Configuro RTCC
   ****************************************************************************/
  EECON2=0x55;                                                                //codigos de seguridad                          
  EECON2=0xaa;        
  RTCCFGbits.RTCWREN= 1;                                                      // desHabilito escritura RTCCONF    

  while(!RTCCFGbits.RTCWREN);

  RTCCFGbits.RTCEN= 0;                                                        // RTCC ENABLE

  while(RTCCFGbits.RTCSYNC);   

  RTCCFGbits.RTCOE= 1;                                                        // salida de rtcc con fines de prueba   

  PADCFG1bits.RTSECSEL0= 0; //0x02                                            // 0:pulso de alarma 1:second 2:osc2 selecciono que sale por pin RTCC seconds clock out    
                                                                              // seguridad, evita falsas alarmas
  //Alarm 
  ALRMCFGbits.AMASK= 2;                                                       // 10  seg                
  ALRMCFGbits.CHIME= 1;                                                       // repeticion indefinida de alarmas    
  ALRMCFGbits.ALRMPTR= 0;
  ALRMVALL=0x01;
  ALRMCFGbits.ALRMEN= 1;

  RTCCFGbits.RTCEN= 1;    
  while(!RTCCFGbits.RTCEN);

  RTCCFGbits.RTCWREN= 0;                                                      //bloqueo

  while(RTCCFGbits.RTCWREN);

  /***************************************************************************
   * FIN de Configuro RTCC
   ***************************************************************************/
  ADCON0bits.ADON=1;

  RCONbits.IPEN= 0;                                                           //desabilita interrup prioridad        

  //PIE3bits.RTCCIE= 1;                                                       //RTCC Interrupt enable PARA DESPERTAR SLEEP
  //PIR3bits.RTCCIF= 0;

  PIE2bits.OSCFIE= 0;                                                         //borro si hubo error de osc    
  PIR2bits.OSCFIF=0;

  INTCON=Intcon;

  return;
}

void set_ints(void)
{
  /******************************************************************
   * Funtions to attent a int sensor and perifheral from libraries
  *******************************************************************/
  set_int_RTCC();
  Set_int_Sense();
  
  INTCONbits.PEIE= 1;
  while(RTCCFGbits.RTCSYNC);
}

void set_int_RTCC(void)
{
  PIE3bits.RTCCIE= 1;                                                         //RTCC Interrupt enable PARA DESPERTAR SLEEP
  PIR3bits.RTCCIF= 0;
}

void deepSleep(void)
{
  OSCCONbits.IRCF= 0;                                                         // OSC 31khz queda fijo solo cambio fuente primaria
  OSCCONbits.SCS= 3;                                                          // primary poscaler internal clock prescaler, CONFIG2L FOSC=01 primary clock source cpu driver
  
  DSCONHbits.DSEN= 0;                                                         //slepp and SEC_RUN:0 deep sleep: 1
  Sleep();                                                                    //  
}
  

typedef enum ALARM_t_def
  {
      ALARM_RTCC=1,
      ALARM_SENSE,
  }ALARM_t;

ALARM_t irq_WakeUp(void){
  return(ALARM_SENSE);
}

typedef enum SENSOR_t_def
  {
      PROXIMITY=1,
      AMBIENTLIGHT,
      ACCELEROMETER,
      MAGNETOMETER,
      ENDSTOP
  }SENSOR_t;

SENSOR_t ControlSense_SENSOR(void){
  return(PROXIMITY);
}

void FunSleep_USER(void)
{
  BYTE Intcon;
  BOOL flag = TRUE;
  //Desactiva INT TMR0 despierta de modo incorrecto al Medidor !!!!
  Intcon= INTCON;                                                             //timer de ieee para randon y SEC
  INTCON=0;
  
  set_ints();
  do{
    /******************************************************************
    * Put here funtion to attent a sensor
    ******************************************************************/
    //control_Sense(); // from sensor.c
    /******************************************************************
    * Configure Sleep type of power management
    ******************************************************************/    
    deepSleep();
    FunAwake();
    switch(irq_WakeUp()){//discrimino fuente de interrupción.
      case ALARM_RTCC:
        flag= FALSE;
        break;
      case ALARM_SENSE:
        //discrimino sensor de interrupción.
        switch(ControlSense_SENSOR())
        {
#ifdef  COUNTER_H
          case PROXIMITY:            
            Measuring();
          break;
#endif          
          
#ifdef  PROXIMITY_H
          case PROXIMITY:            
            //Measuring();
          break;
#endif        
          
 #ifdef AMBIENTLIGHTSENSOR_H
          case AMBIENTLIGHT:
            ambientlight.illuminance;
          break;
#endif                  
#ifdef ACCELEROMETERSENSOR_H         
          case ACCELEROMETER:
            accelerometer.xyz;
          break;
#endif                  
#ifdef MAGNETOMETERSENSOR_H
           case MAGNETOMETER:
            magnetic.xyz;
          break;
#endif                  
          
#ifdef ENDSTOP_H
          case ENDSTOP:
            endstop.stop;
          break;
#endif         
          /*         
          /************************************************************
          * Add here new sensor objets to attent
          *************************************************************/
          /* 
          case ENDSTOP:
            sensor.value;
          break;
          */
          default:
          break;
        }
        flag = TRUE;
        break;
    }
  }while(flag);
   
  INTCONbits.PEIE= 0;
  PIE3bits.RTCCIE= 0;                                                         //RTCC Interrupt enable PARA DESPERTAR SLEEP    
  INTCON=Intcon; 
  return;
}
        
void FunAwake(void)
{
  OSCCONbits.IRCF=7;                                                          //8mhz
  while(!OSCCONbits.OSTS);
  OSCCONbits.SCS=0;                                                           // primary clock source CPU DIVIDER si FOSC 10x 01x 11x  primary clock source cpu driver
  //Sens_En_On();                                                             // enciendo el sensor
  Delay10us(1);                                                               // retardo de despertar el Hall
  ADCON0bits.ADON=1;                                                          
  /* Senso el estado del port hall  Sens_Re */
  //SensState();
}

WORD Read_VBGVoltage(void)
{ 
  ADCON0bits.ADON= 1;
  ADCON0bits.CHS= 15; 	    // Configures the channel as VBG

  ADCON1bits.ADFM= 1;         //
  ADCON1bits.ACQT= 7;
  ADCON1bits.ADCS= 5;
  ANCON1bits.VBGEN= 1;		// Enable Band gap reference voltage

  Delay10us(1000);			//Wait for the Band Gap Settling time

  //calibracion    
  ADCON1bits.ADCAL=1;
  ADCON0bits.GO=1;
  while(ADCON0bits.GO);
  ADCON1bits.ADCAL=0;
  //end calibracion

  PIR1bits.ADIF = 0;
  PIE1bits.ADIE = 0;			//Disable ADC interrupts
                //This routine uses the polling based mechanism
  ADCON0bits.GO = 1;		    //Start A/D conversion    
  while(ADCON0bits.GO);

  ANCON1bits.VBGEN = 0;	    // Disable Bandgap

  return ADRES;
}

BYTE FunADC(void)                                                               //Lectuta estado bateria.
{
    WORD Fix;    
    LONG Vbat;    
    BYTE l;
    
    Fix=Read_VBGVoltage();    
    Fix=(12000/Fix);   
    
    //Configures the channel 0
    ADCON0bits.CHS= 0; 	    
    //interrupciones 
    PIR1bits.ADIF = 0;
    PIE1bits.ADIE = 0;
    
    Vbat=0;
    Delay10us(1);
    for(l=0; l<Lsample; l++)
    {                    
       
        ADCON0bits.GO=1;

        while(ADCON0bits.GO);

        Vbat+=ADRES;
                    
        DelayMs(1);
    }    
          
    //Filtrado por promedio   
    Vbat=Vbat/Lsample;
    //Ajuste por Fix Vgap
    Vbat=Vbat*Fix;
    //Unidad:
    l=(BYTE)(Vbat/10000);
    l&=0x0f;
    l<<=4;
    //decima:
    Vbat=Vbat%10000;    
    l|=(BYTE)(Vbat/1000);
    //Redondeo de centecimas a decima    
    /*Vbat=(Vbat%1000);
    if (Vbat>500)
        if((l&0x0f)>8){
            l+=0x10;
            l&=0xf0; 
        }else
            l++;
    */
    ADCON0bits.ADON=0;
    return (l);
    
}
/*
WORD ReadTmr3(void)
{                                                                               //lee estado del contador de eventos del medidor
    WORD h,l;
    
    l=(WORD)TMR3L;                                                                        
    h=(WORD)TMR3H;
    h=h<<8;
    h=h|l;
    return(h);
}
*/   
DATE_M_t * FunReadRTCC(void)                                                    //leo datos BCD    
{
    DATE_M_t *p;
       
    INTCONbits.GIE=0;
    EECON2=0x55;
    EECON2=0xAA;
    RTCCFGbits.RTCWREN=1;                                                       //habilito escritura pointer RTCC
    
    while(RTCCFGbits.RTCSYNC);
    
    //ubico PNTER    
    RTCCFGbits.RTCPTR0=0;
    RTCCFGbits.RTCPTR1=1;
    
    p->Day=RTCVALL;
    p->Mon=RTCVALH;
    p->Hou=RTCVALL;
    //a=RTCVALH;
    //Date.Seg=RTCVALL;
    //Date.Min=RTCVALH;
    
    
    RTCCFGbits.RTCWREN=0;
    INTCONbits.GIE=1;
    return(p);
}   

DATE_CLK_t *FunReadRTCC_Ext(void)                                              //leo datos BCD    
{
    
    DATE_CLK_t *p=NULL;
    BYTE a;
    p=&App.DateTx;  
    
    INTCONbits.GIE=0;   
    EECON2=0x55;
    EECON2=0xAA;
    RTCCFGbits.RTCWREN=1;                                                        //habilito escritura pointer RTCC
    
    RTCCFGbits.RTCPTR0=0;
    RTCCFGbits.RTCPTR1=1;
    while(RTCCFGbits.RTCSYNC);                                                  //aguarda hasta que sea valida la lectura
    
    p->Day=RTCVALL;
    p->Mon=RTCVALH;
    p->Hou=RTCVALL;
    a=RTCVALH;//(WeekDay)Requiero R/W para avanzardia de la semana
    p->Seg=RTCVALL;
    p->Min=RTCVALH;
        
    RTCCFGbits.RTCWREN=0;
    INTCONbits.GIE=1;
    
    return(p);
}

void FunAlarm(void)                                             
{   
    //App.Bat=0; //clear
    App.Bat=FunADC();                                                                 
    
    //Tiempo real             
    FunReadRTCC_Ext();                                                        
    /*App.DateTx.Seg=p->Seg;
    App.DateTx.Min=p->Min;
    App.DateTx.Hou=p->Hou;
    */
    return;
}
 

void Fun_Rtcc_Fix(void)                                                                 //funcion de calibracion de Reg RTCC
{
    BYTE a; 
    INTCONbits.GIE=0;
        
    //habilito escritura pointer RTCC
    EECON2=0x55;
    EECON2=0xAA;
    RTCCFGbits.RTCWREN=1; 
    
    while(!RTCCFGbits.RTCWREN);
    //ubico PNTER 
    RTCCFGbits.RTCPTR0=1;
    RTCCFGbits.RTCPTR1=1;
    RTCCFGbits.RTCEN= 0;  
    while(RTCCFGbits.RTCSYNC);
    //ubico PNTER 
   
    RTCVALL=App.DateTx.Yea;
    //RTCVALH;//(Reserved) !!!No requiere W/R para avanzar
    RTCCFGbits.RTCPTR0=0;
    RTCCFGbits.RTCPTR1=1;
    RTCVALL=App.DateTx.Day;
    RTCVALH=App.DateTx.Mon;
    RTCVALL=App.DateTx.Hou;
    a=RTCVALH;//(WeekDay)requiere W/R para avanzar
    RTCVALL=App.DateTx.Seg;
    RTCVALH=App.DateTx.Min;
    
    while(RTCCFGbits.RTCSYNC); 
    RTCCFGbits.RTCEN= 1; 
    
    RTCCFGbits.RTCWREN=0;
    while(RTCCFGbits.RTCWREN);
     
    INTCONbits.GIE=1;
    
    return;
}

BYTE BcdtoByte(BYTE Bcb)
{
    BYTE By;
    
     By=((Bcb&0xf0)>>4)*10;
     By+=Bcb&0x0f;
    return(By);
}


WORD Fun_Count_Fix(WORD Cnt)
{
    DATE_CLK_t *p=NULL;
    WORD Slt=0,V;    
    
    /*los slots se repetien cada 6 horas, existen 3 tx diarias cada 8 hs*/
    //Tiempo real          
    
    p=FunReadRTCC_Ext();
    
    V=BcdtoByte(p->Seg);
    if (V>9)
    {
        Slt+=V/10; 
        //0  |10  |20  24   29| el 24seg se ecuentra en el slot 2, pero la division resulta 2
    }              
    
    Slt+=BcdtoByte(p->Min)*6;
    Slt+=(BcdtoByte(p->Hou)%8)*360; //resto division entera
    
    return(Slt);
}

 BOOL FunRTCC( RTCCState_t RTCCState, BOOL St_Tx, BOOL Clr)                     //funcion que llama la interrupcion            
{
    #define MX_SLOT     2880
    #define BORDER_SLOT 2591
    static BYTE i=0,c=0;
    static WORD Count_Slot;
    static BOOL f=TRUE;
    static BOOL New=TRUE;
    BOOL a;
    
    //Determino que si transmitir exitoso(st_tx) y ndistinto por DIF o NORM (a))
    if (Clr)                                                            
        i=0; // reinicio contador de horas/indice medida.                         
    
    if(New){ //Entra en el solo luego del Reset
        
        Count_Slot=Fun_Count_Fix(Count_Slot);
        App.SlotDif=BORDER_SLOT;
        App.SlotDif+=(WORD)TMRL;
        App.SlotDif+=(WORD)(TMRL>>3);
        New=FALSE;        
        Counter=0;
        for (i=0;i< LengMesure;i++ )
            App.MedidaDif.MedidaD[i]=0;
        i=0;
    }
    
    FunReadRTCC_Ext();
    
    switch(RTCCState)
    {
        case RTCC_ON:                                                           //cuenta real lo hace de 0-9 entonces 9 seg con la mascara
                                    
/*******************Registro el consumo aqui por hora**************************/            
                
            if (BcdtoByte(App.DateTx.Min)>58) //arma estructura con los datos del periodo 
            {   
                if (f) //Registra si entro anteriormente si >58                 //Entra una sola vez
                {                    
                    if (i==LengMesure)
                    { //llega al fin del vector                                  //solo 24 elementos tiene el vector.        
                        for(i=0;i<LMmuno;i++) // [i] finaliza en LMmdos
                            App.MedidaDif.MedidaD[i]=App.MedidaDif.MedidaD[i+1];//corro una muestra es una pila de 24       
                    }
                    App.MedidaDif.Index=i;                                      // apunta ultima medida disponible        
                    App.MedidaDif.MedidaD[i++]=0;//(BYTE)(Counter-App.MedidaRE.Mesure);  //Medidor puerde ser variable <count>
                    App.MedidaRE.Mesure=Counter;                                                                                         
                    App.MedidaRE.DateMesu.Hou=App.DateTx.Hou;    //Tiempo real               
                    App.MedidaRE.DateMesu.Day=App.DateTx.Day;
                    App.MedidaRE.DateMesu.Mon=App.DateTx.Mon;                    

                    f=FALSE;                     
                }
                                                
            }else
                f= TRUE;
                
/******************************************************************************/                                
         
            if(St_Tx)
            {
                if(1) //(Count_Slot== App.Slot) // && (c==0))                   //Primera comunicacion con padre asigna el Slot multiplos por falla
                {   
                    /*
                    mide bateria y fecha de tx
                    */
                    FunAlarm();                                                 //completa datos para tx Entrega control para la comunicacion
                    a= TRUE; //Dispara para que se TX los datos                                                       //indica que debo realizar la TX en el retorno
                }
            }else
            {                
                if (1)//(Count_Slot==App.SlotDif && Count_Slot)                 //difiere inicio de contiende todos contra todos Aleatorio
                { 
                    /*
                    mide bateria y fecha de tx
                    */
                    FunAlarm();                                                 //completa datos para tx Entrega control para la comunicacion
                    a= TRUE; //Dispara para que se TX los datos                                                   //indica que debo realizar la TX 
                }     
            }                        
          break;

        case RTCC_FIX:                                                          
            /*funcion de calibracion de Reg RTCC*/            
            Fun_Rtcc_Fix();                                                                   
            /*funcion de calibracion de count que ajuste al slot segun RTCC*/
            Count_Slot=Fun_Count_Fix(Count_Slot);
            //ASINGO DE FORMA ALEATORI A Diff
            App.SlotDif=BORDER_SLOT;
            App.SlotDif+=(WORD)TMRL;
            App.SlotDif+=(WORD)(TMRL>>3);                                    
            a= TRUE;
          break;
    }
    
    if (Count_Slot == MX_SLOT)                                                    //cada 8 hs                  
        Count_Slot= 0;
    else
        ++Count_Slot;
    
    return(a);
}
 