/* 
 * File:   Sensor.h
 * Author: cba
 *
 * Created on December 15, 2023, 9:57 AM
 */

#ifndef SENSOR_H
#define	SENSOR_H

/**
  Generated API interface Manager Source File for Sensors

  @Summary:
    
  @PreCondition:
    
  @param:
    This is Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @return:
    This header file provides implementations for global  handling.
    For individual peripheral handlers please see the peripheral driver for
    all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.65.2
        Device            :  PIC18F26K20
        Driver Version    :  1.
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.45, C18 , C ANSI        
*/

#include "GenericTypeDefs.h"


//Sensor intance

enum Sense_t{
  AbsoluteOrientationSensor=1,
  AccelerotionSensor,
  AmbientLightSensor,
  GravitySensor,
  GyroscopeSensor,
  LinearAccelerationSensor,
  MagneticSensor,
  CompasSensor,
  OrientationSensor,
  RelativeOrientationSensor,
  EndstopSensor,
  PulseSensor,
  RadiationSensor,
  RotarySensor,
  RadiometricSensor,
  DistanceSensor,
  ProximitySensor,
  WeightSensor,
  AltitudeSensor,
  PresureSensor,
  FlowSensor,
  TemperatureSensor,
  WindSensor,
  HumiditySensor,
  PluvicSensor,
};

enum Tech_t{
  Hall=1,
  Optic,
  Laser,
  Resistive,
  Inductive,
  Capacitive,
  Coriolis,
  Ultrasound
};

typedef enum   Sense_t Sense;
typedef enum   Tech_t Tech;

struct SensorVtbl_t
{
  BOOL (*activated) (void);
  BOOL (*hasReading)(void);
  void (*timestamp) (void);
  
  void (*start)    (void);
  void (*stop)     (void);

  void (*activate)(void);
  void (*error)    (void);
  void (*reading)  (void);
};

typedef struct SensorVtbl_t SensorVtbl;

struct Sensor_t{
  /* Standar Methods */
  const SensorVtbl *vptr;
  /* Standar Properties */  
  const Sense *type_s;
  // Statues
  BOOL activated;
  BOOL hasReading;
  unsigned int  timestamp;
  // Events
  BOOL activate;
  BOOL error;
  BOOL reading;
};

typedef struct Sensor_t Sensor;


/********************************************************************
 * Instance properties
 ********************************************************************/
BOOL Sensor_activated(Sensor *const);
BOOL Sensor_hasReading(Sensor *const);
void Sensor_timestamp(Sensor *const);
/********************************************************************
 * Instance methods
 ********************************************************************/
void Sensor_start(Sensor *const);
void Sensor_stop(Sensor *const);
//constructor
void Sensor_setInterface(SensorVtbl *);
/********************************************************************
 * Events
 ********************************************************************/
void Sensor_onActivate(Sensor *const);
void Sensor_onError(Sensor *const);
void Sensor_onReading(Sensor *const);



#endif	/* SENSOR_H */

