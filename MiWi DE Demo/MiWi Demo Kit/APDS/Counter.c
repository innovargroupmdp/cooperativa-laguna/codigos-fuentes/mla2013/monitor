/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#endif

//#include <stdio.h>
//#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "GenericTypeDefs.h"
#include "TimeDelay.h"
#include "APDS9930.h"
#include "APDS.h"          /* User funct/params, such as InitApp              */
//#include "mcc_generated_files/mcc.h" /* User funct/params, such as InitApp    */
#include "Counter.h"
//#include "mcc_generated_files/test.h"
//#include "mcc_generated_files/buttons.h"

//extern Data_base_Def data_max[4];
//extern struct Int_Def_t val_init[4];
/******************************************************************************/
/* Global Variable Declaration                                                */
/******************************************************************************/

/* i.e. uint16_t <variable_name>; */
//int __C30_UART= 2;

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

void Init_APDS(void){    
  UINT16 v[1];

  init_APDS9930();
  /* Initialize IO ports and peripherals */
  //InitApp();   
  enablePower();
  setMode(PROXIMITY,ON); 
  setProximityGain(PGAIN_1X);
  setLEDDrive(LED_DRIVE_12_5MA);
  setProximityDiode(2);
  /* TODO <INSERT USER APPLICATION CODE HERE> */
  //TRISA = 0;
  wireWriteDataByte(APDS9930_CONFIG, 0);
  setLEDDrive(LED_DRIVE_100MA);
  DelayMs(25);        
  readProximity(v);
  DelayMs(100);
    
  //BUTTON_Enable(BUTTON_S3);
  //BUTTON_Enable(BUTTON_S6);
  //BUTTON_Enable(BUTTON_S5);
  //BUTTON_Enable(BUTTON_S4);  
}    
     

void Measuring(void){
  //__delay_ms(7);
  Take_Measure();   //deberia ser llamadada por un semaforo que asegure el tiempo
  Calculus();       //deberia ser llamadada 
  Process();  
}
  