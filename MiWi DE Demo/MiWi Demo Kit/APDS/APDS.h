#include "GenericTypeDefs.h"

/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/
#define LNGTH            60           // el filtrado reguiere mayor cantidad de muestras  120
#define LNGTH_HN         10           // Longitud de H(n) 
#define NUM_REGSRCH      4            // Cantidad de registros de busqueda   
#define FRCTN_MN         3            // Factor sobre el cual divido LNGTH para el calcula de mean()  resulta en 24-30 muestras 
#define FRCTN_XPLR       1            //denominador para determinar factor de exploracion de superlativos 2
#define FRTR_PRD       ((LNGTH/FRCTN_MN)>>1)
#define FCTR_SRT(a)    ((5*a)/10)      //Tara para valor medio 18�/360� mayor
#define FCTR_AVG(a)    ((5*a)/10)      //Tara para valor medio 18�/360� mayor
#define FCTR_SGM        (0.6) 
#define STD_TRG_LW(a)    ((2*a)/10)    //pondera menor
#define STD_TRG_HG(a)    ((2*a)/10)    //pondera mayor

#define DTCT_MTHD        2              //1: metodo por diferencial , 2:metodo por promediacion
#define SLT_H            2
#define STD_H            8    // porcentaje histrograma
#define LMT_H            (LNGTH*(STD_H/10))
#define UMBR_MIN_100MA   400
#define UMBR_MIN_50MA    300
#define UMBR_MIN_25MA    100
//#define LGTH_DDS         512
#define TRY_LIMIT        10000   
#define FS_MAX           240   
#define BIAS_DLY         2400

#define IDX_R(a)         (a->idx-1)
#define IDX_W(a)         a->idx 

/* TODO Application specific user parameters used in user.c may go here */
/*Data estructures*/
struct Data_base_Def_t{
       
    UINT16 D[LNGTH];    
    BOOL   roll;    
    UINT16 idx;    
};

struct Data_base_Rdc_Def_t{
       
    UINT16 D[2];    
    BOOL   roll;    
    UINT16 idx;    
};

struct Int_Def_t{
    
    UINT16 Mean_std;
    UINT16 sigma_std;    
};

enum state_base_def { init=0, take , compare};
typedef enum state_base_def state_base;

struct Data_base_Period_Def_t{

    UINT32 time[3];
    UINT32 time_D;
    UINT8 idx;
    UINT32 T;
    UINT32 D;
    state_base st;
    BOOL st_D;
};

struct Data_base_Spltv_Def_t{

    UINT32 max;
    UINT32 min;        
};

typedef struct  Data_base_Period_Def_t Data_Period_Def; 
//typedef struct  Int_Def_t
typedef struct  Data_base_Def_t        Data_base_Def;
typedef struct  Data_base_Rdc_Def_t    Data_base_Rdc_Def;
typedef struct  Data_base_Spltv_Def_t  Data_base_Spltv_Def;

//enum state_base_def { init=0, take , compare};
enum tipo_base_def { err=0, ok , half};

//typedef enum state_base_def// state_base;
typedef enum tipo_base_def  tipo_base;
/******************************************************************************/
/* User Function Prototypes                                                   */
/******************************************************************************/

/* TODO User level functions prototypes (i.e. InitApp) go here */

void InitApp(void);         /* I/O and Peripheral Initialization */
void delay(void);


UINT16 Idx_R(Data_base_Def *);
UINT16 Idx_W(Data_base_Def *);
UINT16 Idx_E(Data_base_Def *);

void yhn(Data_base_Def *,Data_base_Def *);    
UINT16 mean (Data_base_Def *, UINT8 );

void Set_Decim(void);
BOOL Decim(UINT8);
UINT8 Inlet_Pulse(Data_Period_Def *);
BOOL Check_Frec(void);
void Take_Measure(void);



//void Set_Act_Chk(UINT8 j);
//void Clr_Act_Chk(void);

void Set_Scheduler_Xplorer(UINT8);
BOOL Ckeck_Scheduler_Xplorer(UINT8);
void Clr_Scheduler_Xplorer(UINT8);
void Set_Xplorer(UINT8);
Data_base_Spltv_Def * Xplorer(void);
void Add_Pulse(UINT8);
void Process(void);
UINT8 Action_Out(UINT8);
void Calculus(void);

//uint16_t * Hist( Data_base_Def* );
