/* 
 * File:   Counter.h
 * Author: cba
 *
 * Created on December 16, 2020, 12:10 PM
 */

#ifndef COUNTER_H
#define	COUNTER_H

void Init_APDS(void);
void Measuring(void);

#endif	/* COUNTER_H */
