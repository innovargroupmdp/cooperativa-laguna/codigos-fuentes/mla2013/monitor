/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__PIC24E__)
    	#include <p24Exxxx.h>
    #elif defined (__PIC24F__)||defined (__PIC24FK__)
	#include <p24Fxxxx.h>
    #elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
    #endif
#endif

#include <stdio.h>
//#include <stdint.h>        /* For UINT32 definition */
//#include <stdBOOL.h>       /* For TRUE/FALSE definition */

//#include "system.h"        /* System funct/params, like osc/peripheral config */
//#include <libpic30.h>      /* Includes TRUE/FALSE definition                  */
#include "GenericTypeDefs.h"
#include "TimeDelay.h"
#include "APDS9930.h"
#include "APDS.h"           /* User funct/params, such as InitApp              */
#include "APDS_mem.h"       /* User funct/params, such as InitApp              */
//#include "mcc_generated_files/mcc.h" /* User funct/params, such as InitApp    */
//API SENSOR
#include "../Proximity.h"  

//#include "mcc_generated_files/test.h"
//#include "mcc_generated_files/DDS.h"
#include "mcc_generated_files/buttons.h"

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

#pragma udata bank2= 0x200
Data_base_Def data_0;    //120 +3
Data_base_Def data_1;    //120 +3
#pragma udata
#pragma udata bank3= 0x300
Data_base_Def data_2;    //120 +3
Data_base_Def data_3;    //120 +3   
#pragma udata

#pragma udata bank4= 0x400
Data_base_Def data_max_0;
Data_base_Def data_max_1;
#pragma udata 
#pragma udata bank5= 0x500
Data_base_Def data_max_2;
Data_base_Def data_max_3;
#pragma udata 

#pragma udata bank6= 0x600
Data_base_Def data_mean_0;
Data_base_Def data_mean_1;
#pragma udata
#pragma udata bank7= 0x700
Data_base_Def data_mean_2;
Data_base_Def data_mean_3;
#pragma udata

#pragma udata bank8= 0x800
Data_base_Def     data_sesgo;
Data_base_Rdc_Def data_sigma;
#pragma udata

#pragma udata bank9= 0x900
Data_Period_Def  T_Glb[NUM_REGSRCH];
BOOL             Scheduler_Glb[NUM_REGSRCH];
BOOL             Xplorer_Glb[NUM_REGSRCH];
struct Int_Def_t val_init[NUM_REGSRCH];
#pragma udata

#pragma idata
BOOL          Stt_Act_Glb[NUM_REGSRCH]= {0,0,0,0};
UINT32        Sample_Glb= 0;
UINT32        Pulse_Glb=  0;
BOOL          Decim_v[NUM_REGSRCH-1]= {0,0,0};
Data_base_Def * v_p_data[NUM_REGSRCH]= {&data_0,&data_1,&data_2,&data_3};
Data_base_Def * v_p_data_max[NUM_REGSRCH]= {&data_max_0,&data_max_1,&data_max_2,&data_max_3};
Data_base_Def * v_p_data_mean[NUM_REGSRCH]= {&data_mean_0,&data_mean_1,&data_mean_2,&data_mean_3};

#pragma idata

#pragma udata
Proximity proximity;
#pragma udata


//char string[6];

/* <Initialize variables in user.h and insert code for user algorithms.> */

/* TODO Initialize User Ports/Peripherals/Project here */
void InitApp(void){
    
  UINT16 i= 0;
  UINT8  j= 0;
  const Sense type_s = ProximitySensor;
  const Tech type_t= Optic;

  //UINT8  k=0;

  //Sample_Glb= 0;
  proximity_init(&proximity, type_s, 1, type_t );
  
  data_sesgo.idx=0;
  data_sesgo.roll=0;
  data_sesgo.D[0]  =0;

  data_sigma.idx=0;
  data_sigma.roll=0;
  data_sigma.D[0]  =0;

  for(j=0; j<4;j++)
  {
    Scheduler_Glb[j]= 0;   

    T_Glb[j].T=    0xffffffff;
    T_Glb[j].D=    0xffffffff;
    T_Glb[j].idx=  0;
    T_Glb[j].st= init;     
    T_Glb[j].st_D= 0;
    T_Glb[j].time_D= 0;

    v_p_data[j]->idx=   0;
    v_p_data[j]->roll=  0;

    v_p_data_mean[j]->idx=0;
    v_p_data_mean[j]->roll=0;

    v_p_data_max[j]->idx=0;
    v_p_data_max[j]->roll=0;
    i=0;

    do{
      v_p_data[j]->D[i]     =0;
      v_p_data_mean[j]->D[i]=0;
      v_p_data_max[j]->D[i] =0;
      //data_mean_hard.D[i]=0;
    }while (i++<LNGTH);
  }
}

void delay(void) {
    long i = 65535;
    while(i--);
}

/*
 * Idx_R
 * apunta a la direccion de estritura.
 * Siguiente elemento anterior al libre
 */
UINT16 Idx_R(Data_base_Def *i){
    
  Data_base_Def *j;
  j=i;
  if(j->roll)        
      if(j->idx) // id distinto de cero          
          return (j->idx-1);
      else
          return (LNGTH-1);            
  else
      if(j->idx)           
          return (j->idx-1);
      else
          return (0); //returna cero            
        
}
/*
 * Idx_W
 * apunta a la direccion de estritura.
 * Elemento libre
 * IDX apunta al elemento libre para escribir
 */
UINT16 Idx_W(Data_base_Def *i){
    UINT16 a;      
    Data_base_Def *j;
    j=i;
    
    a= j->idx++;
    if(j->roll)
    {
        if(j->idx == LNGTH )
            j->idx= 0;
        //return (a);      
    }           
    else    
        if(j->idx == LNGTH )
        {   
            j->roll= TRUE;        
            j->idx= 0;
            //return (a);      
        }   
    return (a);                  
}

UINT16 Idx_E(Data_base_Def *i){
    
    Data_base_Def *j;
    
    j= i;
    if(j->roll)
    {
        if(!j->idx )
        {   
            j->idx=LNGTH-1;                
            //return (LNGTH-1);      
        }       
    }else
        {
            if(j->idx)
                j->idx--;
            //else
            //  j->idx= 0;
        }
    return (j->idx);                  
}

//       origen          salida
void yhn(Data_base_Def *p,Data_base_Def *q){
    
  Data_base_Def *r,*s;
  static UINT16 mod= 0;
  struct H_Def{
     UINT16 H[LNGTH_HN];
     UINT8 idx;
     BOOL  roll; 
  }Hn;
  UINT16 i,n,j,k;   
  UINT32 sum= 0;

  i=0;
  do{
      Hn.H[i]=0;        
  }while (i++<LNGTH_HN);

  r= p; //base de se?al sin procesar
  s= q; //base de se?al procesada

  n= Idx_R(r);
  if(r->roll)
  {    
    if(n < LNGTH_HN) //n : indice
    {
      //n= Idx_R(p);            
      j= LNGTH_HN - (n+1);  //cantidad de elementos a leer en el roll pasado            
      k= LNGTH - j-1;       //indice aque apunta al elemento antes del roll

      for (i= 0 ; i <= n ; i++)       //leo despues del roll
      {
          Hn.H[i] = r->D[i];            

      }
      j=0;
      for (; i < LNGTH_HN; i++)       //leo antes del roll
      { 

          Hn.H[i] = r->D[k + j];
          j++;

      }
    /*
      for (i = 0; i < j; i++)
          Hn.H[i] = p->D[k + i];

      for (; i < LNGTH_HN; i++)
          Hn.H[i] = p->D[i - (j+1)];            
    */    
    }else
      if(n >= LNGTH_HN-1)
      {                
        //n= Idx_R(p);
        for (i = 0; i < LNGTH_HN ; i++)
            Hn.H[i] = r->D[ (n - LNGTH_HN ) + i];          
      }
  }    
  else        
    if(n >= LNGTH_HN)
    {        
      for (i = 0; i < LNGTH_HN; i++)
          Hn.H[i] = r->D[n - LNGTH_HN + i];
    }    
    else
    {   
      //La salida directa sin procesar, no hay suficiente tramas
      //completa la demora de la salida del filtro
      i= 0;
      while( i <= n)
          sum += r->D[i++];

      sum/=i;
      s->D[Idx_W(s)]= sum;
      return;
    }  

  for(i=0; i<LNGTH_HN; i++)
      sum+= Hn.H[i];

  sum+= mod;
  mod=  sum % LNGTH_HN;    
  sum/= LNGTH_HN;

  s->D[Idx_W(s)]= sum;

  //if(499==q->idx)
  //printf(utoa(string,sum,10));
  //puts(' ');
  //printf(utoa(string,q->idx,10));  
  //puts('\n');

  return;
}

/*extraigo la media en bloques mas grandes de H(n) y variables*/
UINT16 mean (Data_base_Def *q, UINT8 frc){
    
  UINT32 acum= 0;
  UINT16 i= 0;
  UINT16 n,j,k;            
  Data_base_Def *p;

  p= q;

  j= 0;
  k= 0;
  n= Idx_R(p);   
  frc= LNGTH/frc;

  if(p->roll)
  {    
    if(n < frc) //n : indice
    {
        //n= Idx_R(p);            
        j= frc - (n+1);  //cantidad de elementos a leer en el roll pasado            
        k= LNGTH - j-1;       //indice aque apunta al elemento antes del roll

        for (i= 0 ; i <= n ; i++)       //leo despues del roll
        {
            //if (i<LNGTH)
                acum += p->D[i];  

        }
        j=0;
        for (; i < frc; i++)       //leo antes del roll
        {                 
            //if ((k + j)<LNGTH)
            acum+= p->D[k + j];
            j++;               
        }              
    }else
        if(n >= frc)
        {                             
            for (i = 0; i < frc ; i++)
            //    if (((n - frc ) + i)<LNGTH)
                acum+= p->D[ (n - (frc-1) ) + i];          
        }                        
  }    
  else
  {   
    //acum= 0;
    if(n >= (frc-1))
    {        
        for (i = 0; i < frc; i++)
        {   
            //if ((n - (frc-1) + i)<LNGTH)
            acum+= p->D[n - (frc-1) + i];                
        }    
    }    
    else
    {   
        //La salida directa sin procesar, no hay suficiente tramas
        //completa la demora de la salida del filtro
        i= 0;
        while( i <= n )
        {
            acum+= p->D[i++];                                                            
        }

        if(i)
            acum/= i;                        

        return(acum & 0xFFFF);
    }  
  }
  acum/= frc;

  return ((UINT16)acum);
  
}

void Set_Decim(void){
    
    //static UINT8  k = 0;
    static UINT8  k1= 0;
    static UINT8  k2= 0;
    static UINT16 k3= 0;    
    
    if( !k1 )//( !(s%8))             //fs=30
    {
        Decim_v[0]= TRUE;
    }else
        Decim_v[0]= FALSE;

    k1++;
    if(k1 == 40)//8
        k1= 0;

    if( !k2 )//( !(s%80)||(s==0))    //fs=3
    {
        Decim_v[1]= TRUE;
    }else
        Decim_v[1]= FALSE;

    k2++;
    if(k2 == 400) //80
        k2= 0;

    if( !k3 )//( !(s%800)||(s==0))   //fs=0.3
    {
        Decim_v[2]= TRUE;
    }else
        Decim_v[2]= FALSE;

    k3++;
    if(k3 == 4000) //1500//800
        k3= 0;
        
}

BOOL Decim(UINT8 j ){
      
  switch (j)
  {   
    case 0:
        return (TRUE);
    break;

    case 1:
        return(Decim_v[0]);
    break;
    case 2:    
        return(Decim_v[1]);
    break;
    case 3:
        return(Decim_v[2]);
    break;    
  }    
  return(FALSE);
        
}

UINT8 Inlet_Pulse(Data_Period_Def *t){    // t puntero para el vector interno
    
    UINT32 T2;
    //static    UINT32 time[3];
    //static UINT8 i= 0;
    //state_base  st= init;    
    Data_Period_Def *u;
    /*- Si es por ingreso , muestras . Es relativo
      - Si es por tick es absoluto                 */
    
    //time[i++]= Sample_Glb;
    
    u= t;
    
    u->time[u->idx]= Sample_Glb;    
     
    switch (u->st)
    {
        case init:            //i=1
            
            u->st++;
            u->idx=1;
            return(ok);
            
        break;
        case take:            //i=2  
                        
            u->T= u->time[1]- u->time[0];            
            u->st++;
            u->idx=2;    
            return(ok);
            
        break;
        case compare:         //i=3  //regimen permanente
            
            T2= u->time[2]- u->time[1];
            //Add_item("T2",(uint16_t)T2); 
            //Add_item("T1",(uint16_t)u->T);
                        
            if (T2 > (1.8 * u->T))
            {   
                /*SOLICITO NUEVO SCAN*/
                u->idx= 0;
                u->st= init;            
                return(half);
            }
            else if(T2 < (( u->T )>>2)) //LA VELOCIDAD AUMENTO MAS DEL cuadruple
            {
                /*ELIMINO POR ERROR, POSIBLE RUIDO. Este pulso no corresponde*/                
                u->idx= 0;
                u->st= init;
                //puts("<\n");
                return(err);
            }                
            else 
            {                     
                u->T= T2;             // Dejo periodo actual para el futuro
                u->idx= 2;
                u->time[1]= u->time[2];   //                 
                return(ok);
            }                    
            
        break;
    }
    
    //puts('&');
    return(err);    
}

BOOL Check_Frec(void){
    return(FALSE);
}

void Take_Measure(void)
{
    //UINT16 *u;    
    //UINT16 x,y;
    static UINT16 f=0;
    Proximity *prox;
    //static uint16_t  s=0;
    //static BOOL st = FALSE;
    //u= &x;
    Sample_Glb++;
    
    if ( !f ) // cada 10 segundos verifica el sesgo
    {
      /*Driver de Led bajo lumninacia para ver sesgo*/
      //readProximity(u);       //descargo medici�n anterior
      proximity_get(prox);
      
      wireWriteDataByte(APDS9930_CONFIG, 1);
      setLEDDrive(LED_DRIVE_12_5MA);
      DelayMs(100);
      //readProximity(u);
      proximity_get(prox);

      //data_sesgo.D[Idx_W(&data_sesgo)]= *u;
      data_sesgo.D[Idx_W(&data_sesgo)]= prox->proximity;

      /*Driver de Led subo lumninacia */
      wireWriteDataByte(APDS9930_CONFIG, 0);
      setLEDDrive(LED_DRIVE_100MA);
      DelayMs(25);       
      //readProximity(u);     //descargo medicion anterior  
      proximity_get(prox);  
      //LATAbits.LATA2 = ~LATAbits.LATA2;                       
      f= 0;
    }    
    
    f++;
    if(f==800)
        f=0;
    
    //readProximity(u);
    proximity_get(prox);
    
    /*Almaceno*/
        
    Set_Decim();
    v_p_data[0]->D[Idx_W(&v_p_data[0])]= prox->proximity;
    
    if( Decim(1) ) //(!(s%8))     //fs=32
    {
        v_p_data[1]->D[Idx_W(&v_p_data[1])]= prox->proximity;
        //Sample_Glb[1]++;
        
    }
    
    if( Decim(2) )//(!(s%80)||(s==0))    //fs=3.2
    {
        v_p_data[2]->D[Idx_W(&v_p_data[2])]= prox->proximity;
        //Sample_Glb[2]++;     
    }
    
    if( Decim(3) )// (!(s%800)||(s==0))   //fs=0.32
    {
        v_p_data[3]->D[Idx_W(&v_p_data[3])]= prox->proximity;
        //Sample_Glb[3]++;
     
    }
    
    //Add_item("S",Sample_Glb);
    //printf(utoa(string,*u,10));
    //puts('-');    

    //Toma 1
    //printf(utoa(string,*u,10));   
    //printf(utoa(string,val_init.sigma_std,10));
    //puts(' ');
      
}       

void Set_Scheduler_Xplorer(UINT8 j){
    Scheduler_Glb[j]=TRUE;    
}

BOOL Ckeck_Scheduler_Xplorer(UINT8 j){    
    return Scheduler_Glb[j];    
}
void Clr_Scheduler_Xplorer(UINT8 j){
    Scheduler_Glb[j]= FALSE;    
}

void Set_Xplorer(UINT8 j){
    
    /*blanqueo los registros y regulo la velocidad de muestreo de forma escalonada*/
    //uint16_t i;
            
    //data_max[j].reach=0;
    v_p_data_max[j]->idx= 0;
    v_p_data_max[j]->roll=0;
        
    //@TODO en caso de clr limpiar sample para que no haga roll
        
    
    /*
    do{    
        
        data_max[j].D[i]=0;
        i++;
    }while (i<LNGTH);
    */
    /** Depense el tipo de calculuss setear los correspondientes*/
    Stt_Act_Glb[j]= FALSE;  //calculuss returna a almacernar maximos
    Xplorer_Glb[j]= TRUE;

}


Data_base_Spltv_Def * Xplorer(void){
    
  static Data_base_Spltv_Def v[4];
  //static UINT8 st_xplr= 0;
  UINT16       n,min,max;
  UINT8        i,j;   

  //min1= 5000;

  for (j=0; j<4; j++)
  {   
      if (Decim(j))
      {
          min= 1500;
          max= 100;
          // Estimador de ultima medida 
          yhn(&v_p_data[j],&v_p_data_mean[j]);
          n= Idx_R(&v_p_data_mean[j]);            

          if(v_p_data_mean[j]->roll)
          {
              if( (LNGTH/FRCTN_XPLR) < n )  //60 muestras
              {
                  for (i= n-LNGTH/FRCTN_XPLR ; i< n ; i++ )
                  {
                      if (v_p_data_mean[j]->D[i]> max)
                          max = v_p_data_mean[j]->D[i];

                      if ((v_p_data_mean[j]->D[i]< min) && (min > UMBR_MIN_25MA))
                          min =v_p_data_mean[j]->D[i];                
                  }                    
              }
              else
              {            
                  for (i= LNGTH -(LNGTH/FRCTN_XPLR) + n ; i<LNGTH ; i++ )
                  {
                      if (v_p_data_mean[j]->D[i]> max)
                          max = v_p_data_mean[j]->D[i];

                      if ((v_p_data_mean[j]->D[i]< min ) && (min > UMBR_MIN_25MA))
                          min = v_p_data_mean[j]->D[i];
                  }
                  for (i= 0 ; i< n ; i++ )
                  {
                      if (v_p_data_mean[j]->D[i]> max)
                          max = v_p_data_mean[j]->D[i];

                      if ((v_p_data_mean[j]->D[i]< min) && (min > UMBR_MIN_25MA))
                          min = v_p_data_mean[j]->D[i];
                  }
              }
          }else
          {
              if( (LNGTH/FRCTN_XPLR) >= n ) 
              {
                  //Print_items();
                  //La salida directa sin procesar, no hay suficiente tramas
                  //completa la demora de la salida del filtro
                  i= 0;
                  while( i <= n)
                  {                  
                      if (v_p_data_mean[j]->D[i] > max)
                          max = v_p_data_mean[j]->D[i];

                      if ((v_p_data_mean[j]->D[i] < min) && (min > UMBR_MIN_25MA))
                          min = v_p_data_mean[j]->D[i];                         
                      i++;
                  }
              }                
              else 
              {
                  for (i= n-(LNGTH/FRCTN_XPLR) ; i<= n ; i++ )
                  {
                      if (v_p_data_mean[j]->D[i] > max)
                          max = v_p_data_mean[j]->D[i];

                      if ((v_p_data_mean[j]->D[i] < min) && (min > UMBR_MIN_25MA))
                          min = v_p_data_mean[j]->D[i];                
                  }                    
              }                        
          }

          v[j].max= max;
          v[j].min= min;      
      }
  }    
  return(v);
}

enum st_def {s0=0, s1, s2, s3}; 

void Add_Pulse(UINT8 j){

  static  UINT32 T1[4][2]= {{0,0},{0,0},{0,0},{0,0}};
  static BOOL     st1[4]= {0,0,0,0};
  static BOOL     st2[4]= {0,0,0,0};
  static BOOL     st3[4]= {0,0,0,0};
  static enum st_def st= s3; 
  //static uin? st= s3; 

  //T1[j][1]=0;
  //T1[j][1]=0;
  switch(st)
  {
      case s0:

          switch(j)
          {
              case 0:

                  if(st3[s1])  // detecta si proviene de un estado superior
                  {
                      st3[s1]= FALSE;
                      if((Sample_Glb- T1[s1][0]) < 500)
                      {                         
                          Pulse_Glb--;                      
                      }
                  }

                  Pulse_Glb++;
                  //st3[s0]= TRUE;

                  if(!st1[s0])
                  {
                      T1[s0][0]= Sample_Glb;
                      st1[s0]++;
                  }    
                  else
                  {
                      T1[s0][1]= Sample_Glb - T1[s0][0];
                      T1[s0][0]= Sample_Glb;
                      st2[s0]++;
                  }

              break; 

              case 1:
                  if(st2[s0])
                  {
                      if(Sample_Glb- T1[s0][0]< 1680) // caso de frecuencia muy baja que evita que un pulso de baja siguiente lo active
                      {                                
                          if( (Sample_Glb- T1[s0][0]) > 3360 )
                          {
                              Pulse_Glb++;
                              st= s1;
                              st1[s0]= FALSE;
                          }
                      }    
                      else    
                          if( (Sample_Glb- T1[s0][0]) > 3240 )
                          {
                              Pulse_Glb++;
                              st= s1;
                              st1[s0]= FALSE;
                              st2[s0]= FALSE;
                          }
                  }else  // opcion de salida en caso de enclave de estado
                      if ((Sample_Glb- T1[s0][0]) > 6000)
                      {
                          Pulse_Glb++;
                          st= s1;
                          st1[s0]= FALSE;
                          st2[s0]= FALSE;
                      }                                                                                                   
              break;

              case 2:
                  if (st2[s0])                    
                      if ((Sample_Glb- T1[s0][0]) >( 40* T1[s0][1]))                    
                      {
                          Pulse_Glb++;
                          st1[s0]=FALSE;
                          st2[s0]= FALSE;
                          st= s2;
                      }     

              break;    

              case 3:
                  if (st2[s0])
                      if ((Sample_Glb- T1[s0][0]) >( 600* T1[s0][1]))                    
                      {
                          Pulse_Glb++;
                          st1[s0]=FALSE;
                          st2[s0]= FALSE;
                          st= s3;
                      }

              break;

          }

      break;    

      case s1:

          switch(j)
          {
              case 0:
                  Pulse_Glb++;
                  st1[s1]=FALSE;
                  st2[s1]=FALSE;
                  st= s0;
              break; 

              case 1:         

                   if(st3[s2])  // detecta si proviene de un estado superior
                  {
                      st3[s2]= FALSE;
                      if((Sample_Glb- T1[s2][0]) < 18000)
                      {                         
                          Pulse_Glb--;                      
                      }
                  }

                  st3[s1]= TRUE;

                  Pulse_Glb++;                    

                  if(!st1[s1])
                  {
                      T1[s1][0]= Sample_Glb;
                      st1[s1]++;
                  }    
                  else
                  {
                      T1[s1][1]= Sample_Glb- T1[s1][0];
                      T1[s1][0]= Sample_Glb;
                      st2[s1]++;
                  }

              break;

              case 2:                    

                  if(st2[s1])
                  {
                      if(Sample_Glb- T1[s1][0]< 75000) // caso de frecuencia muy baja que evita que un pulso de baja siguiente lo active
                      {                                
                          if((Sample_Glb- T1[s1][0]) > (150000))
                          {
                              Pulse_Glb++;                                
                              st1[s1]= FALSE;
                              st2[s1]= FALSE;
                              st= s2;
                          }
                      }    
                      else    
                          if((Sample_Glb- T1[s1][0]) > (282000))
                          {
                              Pulse_Glb++;                                
                              st1[s1]= FALSE;
                              st2[s1]= FALSE;
                              st= s2;
                          }
                  }else  // opcion de salida en caso de enclave de estado
                      if ((Sample_Glb- T1[s1][0]) > 600000)
                      {
                          Pulse_Glb++;                            
                          st1[s1]= FALSE;
                          st2[s1]= FALSE;
                          st= s2;
                      }

              break;    

              case 3:
                  if (st2[s1])
                      if ((Sample_Glb- T1[s1][0]) > (15*T1[s1][1]))                    
                      {
                          Pulse_Glb++;
                          st1[s1]=FALSE;
                          st2[s1]= FALSE;
                          st= s3;
                      }

              break;                 
          }

      break;

      case s2:
          switch(j)
          {
              case 0:
                  Pulse_Glb++;
                  st1[s2]=FALSE;
                  st2[s2]=FALSE;
                  st= s0;
              break; 

              case 1:         
                  Pulse_Glb++;
                  st1[s2]=FALSE;
                  st2[s2]=FALSE;
                  st= s1;                   
              break;

              case 2:

                   if(st3[s3])  // detecta si proviene de un estado superior
                  {
                      st3[s3]= FALSE;
                      if((Sample_Glb- T1[s3][0]) < 48000)
                      {                         
                          Pulse_Glb--;                      
                      }
                  }

                  st3[s2]= TRUE;

                  Pulse_Glb++;   

                  if(!st1[s2])
                  {
                      T1[s2][0]= Sample_Glb;
                      st1[s2]++;
                  }    
                  else
                  {
                      T1[s2][1]= Sample_Glb- T1[s2][0];
                      T1[s2][0]= Sample_Glb;
                      st2[s2]++;
                  }
              break;    

              case 3:                                        

                  if(st2[s2])
                  {
                      if(Sample_Glb- T1[s2][0]< 444000) // caso de frecuencia muy baja que evita que un pulso de baja siguiente lo active
                      {                                
                          if((Sample_Glb- T1[s2][0]) > (888000))
                          {
                              Pulse_Glb++;                                
                              st1[s2]= FALSE;
                              st2[s2]= FALSE;
                              st= s3;
                          }
                      }    
                      else    
                          if((Sample_Glb- T1[s2][0]) > (864000))
                          {
                              Pulse_Glb++;                                
                              st1[s2]= FALSE;
                              st2[s2]= FALSE;
                              st= s3;
                          }
                  }else  // opcion de salida en caso de enclave de estado
                      if ((Sample_Glb- T1[s2][0]) > 1700000)
                      {
                          Pulse_Glb++;                            
                          st1[s2]= FALSE;
                          st2[s2]= FALSE;
                          st= s3;
                      }

              break;
          }
      break;    
      case s3:
          switch(j)
          {
              case 0:
                  Pulse_Glb++;                    
                  st= s0;
              break; 

              case 1:         
                  Pulse_Glb++;                    
                  st= s1;                   
              break;

              case 2:
                  Pulse_Glb++;                    
                  st= s2;  
              break;    

              case 3:
                  Pulse_Glb++;
              break;
          }
      break;        
  }      
}

void Process(void){
    
  static  UINT32 try[4] = {0,0,0,0};
  //static    UINT32 try1[4] = {0,0,0,0};
  static BOOL     Stt[4] = {0,0,0,0};
  static BOOL     Stt1[4]= {0,0,0,0};
  static BOOL     Stt2[4]= {0,0,0,0};
  static UINT8  St[4]  = {take,take,take,take};
  static UINT32 T1[4]  = {0,0,0,0};
  //static    UINT32 T2[4]  = {0,0,0,0};
  UINT8 j;
  UINT8 i=0;
  //Add_item("S",Sample_Glb); 
  for(j=0; j<4; j++)
  {
      //Add_item("j1",j);  
      if (Decim(j))
      {
          //Add_item("j2",j);    
          switch ( Action_Out(j) )
          {            
              case FALSE:

                  /* no exite pulso detectado, aun se aguarda medicion no asegura
                     que haya sincronismo */

                  if(Stt_Act_Glb[j])  // Espero a que haya pulso detectado inicial arranque luego de reset
                  {
                      Stt1[j]= TRUE;
                  }            
                  else
                  {
                      if (Stt1[j])    // Primer pulso completo luego de un Reset
                      {                       
                          Stt2[j]= TRUE;
                      }            
                  }

                  if(try[j] == TRY_LIMIT)
                  {
                      try[j]= 0;
                      /* define un scan*/                
                      //Set_Xplorer(j);     // Ya incluye Stt_Act= FALSE;
                      Set_Scheduler_Xplorer(j);
                      Stt_Act_Glb[j]= FALSE;
                      //St[j]= take;
                      T_Glb[j].st=  init;
                      T_Glb[j].idx= 0;
                  }

                  if(Stt2[j])             // Funcionamiento regimen permanente
                  {                                                                           
                      if(T_Glb[j].idx == 2)
                      {   
                          St[j]= take;
                          if (Stt_Act_Glb[j]) //Caso de activacion de pulso con remanecia fuera de tiempo
                          {                       
                              /*switch(St[j])
                              {   //@TODO modificar para el blmaqueo de estados cuando se pasa de stt_ACT
                                  case take:  //i=2                          
                                      T1[j]= T_Glb[j].time[1]- T_Glb[j].time[0];
                                      ++St[j];
                                  break;
                                  case compare: 
                              */ 
                                      T1[j]= Sample_Glb - T_Glb[j].time[1];

                                      if (T1[j] > (1.4 * T_Glb[j].T))
                                      {   
                                          /*SOLICITO NUEVO SCAN*/                                
                                          Set_Scheduler_Xplorer(j);
                                          Stt_Act_Glb[j]= FALSE;
                                          St[j]= compare;
                                          T_Glb[j].st=  init;
                                          T_Glb[j].idx= 0;
                                          //try1[j]= try[j];
                                          try[j]= 0;

                                      }                        
                                  //break;    
                              //}   
                          }
                          else  //Caso de no activacion de pulso , demasiado tiempo de espera
                          {   
                              /*switch(St[j])
                              {

                                  case take:          //i=2 

                                      T1[j]= T_Glb[j].time[1]- T_Glb[j].time[0];
                                      ++St[j];
                                  break;
                                  case compare: 
                               */  
                                      T1[j]= Sample_Glb - T_Glb[j].time[1];

                                      if (T1[j] > (1.4 * T_Glb[j].T))
                                      {   
                                          /*SOLICITO NUEVO SCAN*/
                                          Set_Scheduler_Xplorer(j);
                                          Stt[j]= TRUE;
                                          St[j]= compare;
                                          T_Glb[j].st=  init;
                                          T_Glb[j].idx= 0;
                                          //try1[j]= try[j];
                                          try[j]= 0;
                                      }                        
                                  //break;    
                              //}                       
                          }
                      }else 
                          if ((St[j] == compare) && (T_Glb[j].idx >0))                                                    
                          {                                
                              T1[j]= Sample_Glb - T_Glb[j].time[0];

                              if (T1[j] > (1.4 * T_Glb[j].T))
                              {   
                                  try[j]= 0;
                                  /* define un scan*/                
                                  Set_Scheduler_Xplorer(j);
                                  //Set_Xplorer(j);     // Ya incluye Stt_Act= FALSE;
                                  Stt_Act_Glb[j]= FALSE;
                                  //St[j]= take;
                                  T_Glb[j].st=  init;
                                  T_Glb[j].idx= 0;
                              }
                          }  

                      if (Stt_Act_Glb[j]) //Caso de activacion de pulso con remanecia de estado activo fuera del ultimo
                      {  
                          T1[j]= Sample_Glb - T_Glb[j].time_D;
                          if((T_Glb[j].D<<2) < T1[j])//(T_Glb[j].D<<2) < (Sample_Glb - T_Glb[j].time_D))
                          {
                              try[j]= 0;
                              /* define un scan*/                
                              Set_Scheduler_Xplorer(j);
                              //Set_Xplorer(j);
                              T_Glb[j].st_D = TRUE;
                              Stt_Act_Glb[j]= FALSE;                                
                              //St[j]= take;
                              //T_Glb[j].st=  init;
                              //T_Glb[j].idx= 0;
                              //puts('>\n');
                          }
                      }

                  }       

                  try[j]++;
                  //puts('2');
                  //Print_items(); 

              break;    
              case ok:
                  //puts('3');
                  /*realiz una suma exitosa de pulso de medicion */
                  Add_Pulse(j);
                  //Print_items();
                  i++;
                  try[j]= 0;
                  //St[j]= take;

              break;    
              case half:       
                  //puts('4'); // La frecuencia se redujo a la mitad
                  /*realiza la sintonizacion del registro*/
                  Add_Pulse(j);
                  //Print_items();
                  i++;
                  try[j]= 0;                    
                  St[j]= compare;
                  Set_Scheduler_Xplorer(j);                    

              break;    
              case 0xff:        //error se filtra el pulso la frecuencia aumenta X 2
                  //puts('5');
                  //Print_items(); 
                  /*realiza la sintonizacion del registro*/
                  try[j]= 0;
                  St[j]= compare;                    
                  Set_Scheduler_Xplorer(j);

              break;
          }                               

          //Add_item("i",i);
          if(Stt_Act_Glb[j]) //AGUARDO HASTA QUE HAYA DETECCION PARA PODER HACER EL SCAN
          {
              Stt[j]= TRUE;
          }            
          else
          {
              if (Stt[j])
              {
                  if (Ckeck_Scheduler_Xplorer(j))
                  {    
                      Set_Xplorer(j);                        
                      Clr_Scheduler_Xplorer(j);
                  }    
                  Stt[j]= FALSE;
              }            
          }
      }            
      //clear_items(); 
  }  
  //Print_items();
}

UINT8 Action_Out(UINT8 j){
    
    static BOOL st[4]= {0,0,0,0};
    static BOOL st1[4]= {0,0,0,0};      
    static BOOL i[4]= {0,0,0,0};    
    static UINT8 times=0;
    static UINT8 k=0;
    UINT8 t=0;
    //static UINT8 rev=0;
    UINT8 r= FALSE;
    
    //static UINT8 xstd_low= 1; //STD_TRG_LW; //sigma para caso stado No Activo
    //static UINT8 xstd_high= 1;//STD_TRG_HG; //sigma para caso stado Activo    
    
    //Toma 6
    //printf(utoa(string, (0.3*val_init.sigma_std),10));
    //puts(' ');
    //printf(utoa(string, val_init.Mean_std - STD_TRG_HG(val_init.sigma_std),10));
    //puts(' ');
    //Toma 7
    //printf(utoa(string, val_init.Mean_std + STD_TRG_LW(val_init.sigma_std),10));
    //puts(' ');
    //printf(utoa(string,i,10));
    
    
    //Add_item("l-",val_init[j].Mean_std - STD_TRG_HG(val_init[j].sigma_std));
    //Add_item("l+",val_init[j].Mean_std + STD_TRG_LW(val_init[j].sigma_std) ); //-0.2 *(val_init.sigma_std));    
        
    
    if((val_init[j].Mean_std > UMBR_MIN_25MA) && (val_init[j].Mean_std<0x0FFF))
    {                               
        if ( st[j] )
        {   
            if (v_p_data_mean[j]->D[Idx_R(&v_p_data_mean[j])] >= (val_init[j].Mean_std + STD_TRG_LW(val_init[j].sigma_std)))
            {    
                st[j]= FALSE;
                Stt_Act_Glb[j]= FALSE;
                //xstd_high = STD_TRG_HG;
                //xstd_low  = 100;//STD_TRG_LW;
                // puts('>');
                i[j]=0;
            }           
        }            
        else
            if (v_p_data_mean[j]->D[Idx_R(&v_p_data_mean[j])] <= (val_init[j].Mean_std - STD_TRG_HG(val_init[j].sigma_std)))
            {                   
                //puts('<');
                st[j]= TRUE;
                Stt_Act_Glb[j]= TRUE;               
    
                switch (Inlet_Pulse(&T_Glb[j]))   //contabliza distancia entre pulsos
                {   
                    case err:
                            //puts('>');
                            r= 0xff;// error debo explorar
                            //Stt_Act_Glb[j]= FALSE;
                    break;    
                    case ok:
                            //Add_Pulse(0);
                            r= TRUE;                        // 1
                            i[j]^= TRUE;                                                         
                    break;
                    case half:
                            //Add_Pulse(0);
                            //puts('*');
                            r= half;// error debo explorar  // 2                                         
                            i[j]^= TRUE;                                                          
                    break;
                }                
                //xstd_high = STD_TRG_LW;
                //xstd_low  = STD_TRG_HG-40;                                        
            }
    }
    
    if(st[j])//Stt_Act_Glb[j])  //contabiliza dutty pulse
    {
        if (!st1[j])
        {                        
            T_Glb[j].time_D= Sample_Glb;                                         
            st1[j]= TRUE;
        }
        if(T_Glb[j].st_D && (v_p_data_mean[j]->D[Idx_R(&v_p_data_mean[j])] < (val_init[j].Mean_std + STD_TRG_LW(val_init[j].sigma_std))))
        {   //caso de disparo de scan en process y el pulso activo verdadero
            //sigue en curso debe aportar informacion.
            T_Glb[j].st_D= FALSE;
        }
        
    }else
        if(st1[j])
        {
            if(!T_Glb[j].st_D)  //caso de disparo de scan en process no aporta informacion a dutty              
                T_Glb[j].D= Sample_Glb - T_Glb[j].time_D;
                            
            T_Glb[j].st_D= FALSE;
            st1[j]= FALSE;
        }
    t=0;
    
    if ( BUTTON_IsPressed(BUTTON_S3)){k++;}else
       
        if ( BUTTON_IsPressed(BUTTON_S4)){k--;}
    if(j==k)
    {    
        //Add_item("P",data_mean[j].D[Idx_R(&data_mean[j])]);
//        if((val_init[j].Mean_std > UMBR_MIN_25MA) && (val_init[j].Mean_std<0x0FFF))
        //    Add_item("M-", (val_init[j].Mean_std - STD_TRG_HG(val_init[j].sigma_std)) );
        //Add_item("M+", (val_init[j].Mean_std + STD_TRG_HG(val_init[j].sigma_std)) );
        switch (k)
        {
                case 0:                       
                        while(t<4)
                        {
                            if (i[t])
                                times+=(4-t);        

                            switch (t)
                            {
                                    case 0:
                                        //Add_item("O", (100+100*times));
          //                              Add_item("O", (500+100*i[0]));
                                    break;
                                    case 1:
          //                              Add_item("O1",(500+75*i[1]));
                                    break;
                                    case 2:
          //                              Add_item("O2",(500+50*i[2]));
                                    break;
                                    case 3:
          //                              Add_item("O3",(500+25*i[3]));
                                    break;
                            }
                            t++;
                        }    
                break;
                case 1:
          //          Add_item("O1",(500+75*i[1]));
                break;
                case 2:
          //          Add_item("O2",(500+50*i[2]));
                break;
                case 3:
          //          Add_item("O3",(500+25*i[3]));
                break;
                default:
                    k=0;
        }
        //Add_item("T",(10*Pulse_Glb));
        //if((val_init[j].Mean_std > UMBR_MIN_25MA) && (val_init[j].Mean_std<0x0FFF))
        //    Add_item("M-", (val_init[j].Mean_std - STD_TRG_HG(val_init[j].sigma_std)) );
        //Add_item("M+", (val_init[j].Mean_std + STD_TRG_HG(val_init[j].sigma_std)) );
        //Print_items();
        //clear_items();        
    }
    
   times=0;
   //if((val_init[j].Mean_std > UMBR_MIN_25MA) && (val_init[j].Mean_std<0x0FFF))
   //    Add_item("M-", (val_init[j].Mean_std - STD_TRG_HG(val_init[j].sigma_std)) );
   //Add_item("M+", (val_init[j].Mean_std + STD_TRG_HG(val_init[j].sigma_std)) );
   
    if(st[j] && Stt_Act_Glb[j] )// Deteccion de pulso activo bajo  
    {
        Idx_E(&v_p_data_mean[j]);   // Elimino dato para que no se introduzca en el Max
        //LATA = (0x10<<j) | PORTA ;        
        
        //times+=(4-j);             
    }else
    {
        //LATA = (0xef<<j) & PORTA ;                 
    }
    
    return(r);    
}


void Calculus(void)
{
    Data_base_Spltv_Def *u;
    //uint16_t *p;
    //uint16_t i,n,m,a,b;
    UINT8  j;
    //uint16_t min;
    //static uint16_t min1;
    //uint16_t max;
    static UINT16 f[4]= {2,2,2,2};
    static UINT16 g[4]= {2,2,2,2};//LNGTH/20;
    //static UINT8  st= 2;
    static BOOL     st1[4]= {0,0,0,0};
    
    //for(i=0; (i< LNGTH) & (i< data.idx-1) ; i++)
    //        sum += *(p++);
    
    // Estimador de media con ultimas medidas 
    //yhn(&data,&data_mean);
    
    yhn(&data_sesgo,&data_sigma);
    data_sigma.idx=0; // setero la proxima escritura por se reducido la base
    // Estimador de media dura
    /*
     * if (try==0 || try==100)
    {
        n= Idx_W(&data_mean_hard);
        data_mean_hard.D[n]= mean(&data);
        try=0; 
    }
    try++;
    */
    //p= Hist(&data); //arroja los dos umbrales inferios y superior para el recorte
    
    //Recorto picos de se�al
    /*
    n= Idx_R(&data);
    
    if (data.D[n]< *p)
        data_mark.D[n] = *p;
    else
        if (data.D[n]> *(p+1))
            data_mark.D[n] = *(p+1);
        else
            data_mark.D[n] = data.D[n];
    */
    //Toma 5
    //printf(utoa(string,data_mark.D[n],10));
    //puts('\n');
    //printf(utoa(string,*(p+1),10));
    //puts('\n');

    //Extraigo superlativos cada LNGTH/20
    u= Xplorer();
    
    switch (DTCT_MTHD)
    {   
        case 1:          
            //Extraigo superlativos cada LNGTH/20
                        
            for(j= 0 ; j<4 ; j++)
            {
                if (Decim(j))
                {
                    if ( (u->max - u->min) >= data_sigma.D[Idx_R(&data_sigma)] )
                    {
                        v_p_data_max[j]->D[Idx_W(&v_p_data_max[j])] = u->max;
                        //data_min.D[Idx_W(&data_min)] = min;

                        if (g[j] == 0)
                        {                     
                            g[j]= 2;//LNGTH/FRCTN_XPLR;
                            val_init[j].Mean_std= FCTR_SRT((u->max - u->min))+ u->min;
                            //puts('*');                
                            //puts('*');
                            //LATA = 0x80 | PORTA ;
                           /// if (f == 0)
                           // {     
                               // st= TRUE;
                               // f= 20;
                                ////puts('#');
                           // }
                           /// f--;
                        }
                        g[j]--;                        
                    }  
                    u++;
                }    
            }    
            
        break;
        
        case 2:
        //Extraigo superlativos cada LNGTH/20
             
            for(j= 0 ; j<4 ; j++)
            {
                if (Decim(j))
                {
                    if(!st1[j])
                    {
                        /*    disparo que difine un posible maximo    */
                        if ( (u->max - u->min) > (0.7 * data_sigma.D[Idx_R(&data_sigma)]) )
                        {            
                            v_p_data_max[j]->D[Idx_W(&v_p_data_max[j])]= u->max;
                            //data_min.D[Idx_W(&data_min)]= min;
                            if (g[j] == 0)
                            {
                                g[j]= 2;//LNGTH/FRCTN_XPLR;
                                v_p_data_max[j]->D[Idx_W(&v_p_data_max[j])]= u->max;
                                
                                val_init[j].Mean_std= mean(&v_p_data_max[j],FRCTN_MN);
                                /*
                                if (data_max.D[Idx_R(&data_max)]>data_max.D[Idx_R(&data_max)-1])
                                    val_init.Mean_std+=0.1*(data_max.D[Idx_R(&data_max)]>data_max.D[Idx_R(&data_max)-1]);
                                else
                                    val_init.Mean_std-=0.1*(data_max.D[Idx_R(&data_max)-1]>data_max.D[Idx_R(&data_max)]);
                                */
                                //LATA = 0x80 | PORTA ;
                                DelayMs(500);
                                if (f[j] == 0){
                                   st1[j]= TRUE;                    
                                }   
                    
                                f[j]--;                        
                                //min1= min;                                
                            }
                            g[j]--;                    
                        }
                    }
                    else
                    {
                    
                        if ( ((u->max - u->min) < (0.6 * data_sigma.D[Idx_R(&data_sigma)])) && !Stt_Act_Glb[j] )
                        {
                            v_p_data_max[j]->D[Idx_W(&v_p_data_max[j])]= u->max; 
                            //if(data_max[j].D[Idx_R(&data_max[j])]==0)                                
                            //    puts('<');                        
                            
                            val_init[j].Mean_std= mean(&v_p_data_max[j],FRCTN_MN)- (FCTR_SGM*data_sigma.D[Idx_R(&data_sigma)]);                                                        
                        }
                        else
                        {
                            //@TODO En caso de desenganche tiene que volver al caso anterior de min max busqueda,
                            //se determina por medio de un seguimiento de frecuencia de pulso de deteccion
                            //Tambien un seguimiento por gradiente
                            
                            val_init[j].Mean_std= mean(&v_p_data_max[j],FRCTN_MN)-(FCTR_SGM*data_sigma.D[Idx_R(&data_sigma)]) ;                                                                                  
                        }                        
                    }         
                    
                    val_init[j].sigma_std= data_sigma.D[Idx_R(&data_sigma)];                                                           
                }       
                                
                u++;
                //Add_item("Mx",u->max);
                //Add_item("Sg",val_init[0].sigma_std);
            }                 
            
        break;
        
    }
    //Add_item("Sg",val_init[0].sigma_std);
    //val_init.Mean_std= mean(&data,FRCTN_MN);
    //val_init[j].sigma_std= data_sigma.D[Idx_R(&data_sigma)];
    //Add_item("Md",val_init[j].sigma_std);
    //Add_item("M",val_init[j].Mean_std);
    
    //LATAbits.LATA0++;
}

