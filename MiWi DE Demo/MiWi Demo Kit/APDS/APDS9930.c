/**
 * @file    APDS-9930.cpp
 * @brief   Library for the SparkFun APDS-9930 breakout board
 * @author  Shawn Hymel (SparkFun Electronics)
 *
 * @copyright	This code is public domain but you buy me a beer if you use
 * this and we meet someday (Beerware license).
 *
 * This library interfaces the Avago APDS-9930 to Arduino over I2C. The library
 * relies on the Arduino Wire (I2C) library. to use the library, instantiate an
 * APDS9930 object, call init(), and call the appropriate functions.
 *
 * APDS-9930 current draw tests (default parameters):
 *   Off:                   1mA
 *   Waiting for gesture:   14mA
 *   Gesture in progress:   35mA
 */
   
 #include "APDS9930.h"
 #include "stdio.h"
 #include "string.h"
 #include "stdlib.h"

 #include "mcc_generated_files/i2c1.h"
 #include "mcc_generated_files/uart1.h"
 #include "mcc_generated_files/Api_Wire_I2C.h" 

/**
 * @brief Configures I2C communications and initializes registers to defaults
 *
 * @return TRUE if initialized successfully. FALSE otherwise.
 */
BOOL init_APDS9930(void)
{
    UINT8 *id;
    UINT8 a = 0;
    char string[4];
    /* Initialize I2C */
    //Wire.begin();
    id= &a;
     
    /* Read ID register and check against known values for APDS-9930 */
    if( !wireReadDataByte(APDS9930_ID, id) ) {
        puts("ID bat read \n");
        return FALSE;
    }else
        puts("ID ok read \n");
    
    if( !(*id == APDS9930_ID_1 || *id == APDS9930_ID_2) ) {
        puts("ID bad check \n");        
        return FALSE;
    }else {
        puts("ID ok check \n");
        puts("ID is ");
        puts(itoa(string,*id));
        puts('\n');
    }
     
    /* Set ENABLE register to 0 (disable all features) */
    if( !setMode(ALL, OFF) ) {
        puts("Regs still on\n");
        return FALSE;
    }else
        puts("Regs off\n");
    
    /* Set default values for ambient light and proximity registers */
    if( !wireWriteDataByte(APDS9930_ATIME, DEFAULT_ATIME) ) {
        return FALSE;
    }
    if( !wireWriteDataByte(APDS9930_WTIME, DEFAULT_WTIME) ) {
        return FALSE;
    }
    if( !wireWriteDataByte(APDS9930_PPULSE, DEFAULT_PPULSE) ) {
        return FALSE;
    }
    if( !wireWriteDataByte(APDS9930_POFFSET, DEFAULT_POFFSET) ) {
        return FALSE;
    }
    if( !wireWriteDataByte(APDS9930_CONFIG, DEFAULT_CONFIG) ) {
        return FALSE;
    }
    if( !setLEDDrive(DEFAULT_PDRIVE) ) {
        return FALSE;
    }
    if( !setProximityGain(DEFAULT_PGAIN) ) {
        return FALSE;
    }
    if( !setAmbientLightGain(DEFAULT_AGAIN) ) {
        return FALSE;
    }
    if( !setProximityDiode(DEFAULT_PDIODE) ) {
        return FALSE;
    }
    if( !setProximityIntLowThreshold(DEFAULT_PILT) ) {
        return FALSE;
    }
    if( !setProximityIntHighThreshold(DEFAULT_PIHT) ) {
        return FALSE;
    }
    if( !setLightIntLowThreshold(DEFAULT_AILT) ) {
        return FALSE;
    }
    if( !setLightIntHighThreshold(DEFAULT_AIHT) ) {
        return FALSE;
    }
    if( !wireWriteDataByte(APDS9930_PERS, DEFAULT_PERS) ) {
        return FALSE;
    }
    puts("All sets ok\n");
    return TRUE;
}

/*******************************************************************************
 * Public methods for controlling the APDS-9930
 ******************************************************************************/

/**
 * @brief Reads and returns the contents of the ENABLE register
 *
 * @return Contents of the ENABLE register. 0xFF if error.
 */
UINT8 getMode()
{
    UINT8 *enable_value;
    UINT8 a=0;
    enable_value= &a;
    
    /* Read current ENABLE register */
    if( !wireReadDataByte(APDS9930_ENABLE, enable_value) ) {
        return ERROR;
    }
    
    return *enable_value;
}

/**
 * @brief Enables or disables a feature in the APDS-9930
 *
 * @param[in] mode which feature to enable
 * @param[in] enable ON (1) or OFF (0)
 * @return TRUE if operation success. FALSE otherwise.
 */
BOOL setMode(UINT8 mode, UINT8 enable)
{
    UINT8 reg_val;

    /* Read current ENABLE register */
    reg_val = getMode();
    if( reg_val == ERROR ) {
        return FALSE;
    }
    
    /* Change bit(s) in ENABLE register */
    enable = enable & 0x01;
    if( mode >= 0 && mode <= 6 ) {
        if (enable) {
            reg_val |= (1 << mode);
        } else {
            reg_val &= ~(1 << mode);
        }
    } else if( mode == ALL ) {
        if (enable) {
            reg_val = 0x7F;
        } else {
            reg_val = 0x00;
        }
    }
        
    /* Write value back to ENABLE register */
    if( !wireWriteDataByte(APDS9930_ENABLE, reg_val) ) {
        return FALSE;
    }
        
    return TRUE;
}

/**
 * @brief Starts the light (Ambient/IR) sensor on the APDS-9930
 *
 * @param[in] interrupts TRUE to enable hardware interrupt on high or low light
 * @return TRUE if sensor enabled correctly. FALSE on error.
 */
BOOL enableLightSensor(BOOL interrupts)
{
    
    /* Set default gain, interrupts, enable power, and enable sensor */
    if( !setAmbientLightGain(DEFAULT_AGAIN) ) {
        return FALSE;
    }
    if( interrupts ) {
        if( !setAmbientLightIntEnable(1) ) {
            return FALSE;
        }
    } else {
        if( !setAmbientLightIntEnable(0) ) {
            return FALSE;
        }
    }
    if( !enablePower() ){
        return FALSE;
    }
    if( !setMode(AMBIENT_LIGHT, 1) ) {
        return FALSE;
    }
    
    return TRUE;

}

/**
 * @brief Ends the light sensor on the APDS-9930
 *
 * @return TRUE if sensor disabled correctly. FALSE on error.
 */
BOOL disableLightSensor(void)
{
    if( !setAmbientLightIntEnable(0) ) {
        return FALSE;
    }
    if( !setMode(AMBIENT_LIGHT, 0) ) {
        return FALSE;
    }
    
    return TRUE;
}

/**
 * @brief Starts the proximity sensor on the APDS-9930
 *
 * @param[in] interrupts TRUE to enable hardware external interrupt on proximity
 * @return TRUE if sensor enabled correctly. FALSE on error.
 */
BOOL enableProximitySensor(BOOL interrupts)
{
    /* Set default gain, LED, interrupts, enable power, and enable sensor */
    if( !setProximityGain(DEFAULT_PGAIN) ) {
        return FALSE;
    }
    if( !setLEDDrive(DEFAULT_PDRIVE) ) {
        return FALSE;
    }
    if( interrupts ) {
        if( !setProximityIntEnable(1) ) {
            return FALSE;
        }
    } else {
        if( !setProximityIntEnable(0) ) {
            return FALSE;
        }
    }
    if( !enablePower() ){
        return FALSE;
    }
    if( !setMode(PROXIMITY, 1) ) {
        return FALSE;
    }
    
    return TRUE;
}

/**
 * @brief Ends the proximity sensor on the APDS-9930
 *
 * @return TRUE if sensor disabled correctly. FALSE on error.
 */
BOOL disableProximitySensor()
{
	if( !setProximityIntEnable(0) ) {
		return FALSE;
	}
	if( !setMode(PROXIMITY, 0) ) {
		return FALSE;
	}

	return TRUE;
}

/**
 * Turn the APDS-9930 on
 *
 * @return TRUE if operation successful. FALSE otherwise.
 */
BOOL enablePower()
{
    if( !setMode(POWER, 1) ) {
        return FALSE;
    }
    
    return TRUE;
}

/**
 * Turn the APDS-9930 off
 *
 * @return TRUE if operation successful. FALSE otherwise.
 */
BOOL disablePower()
{
    if( !setMode(POWER, 0) ) {
        return FALSE;
    }
    
    return TRUE;
}

/*******************************************************************************
 * Ambient light sensor controls
 ******************************************************************************/

/**
 * @brief Reads the ambient (clear) light level as a 16-bit value
 *
 * @param[out] val value of the light sensor.
 * @return TRUE if operation successful. FALSE otherwise.
 */
BOOL readAmbientLightLux_Float(float *val)
{
    UINT16 *Ch0;
    UINT16 *Ch1;
    UINT16 a=0;
    UINT16 b=0;
    
    Ch0=&a;
    Ch1=&b;
    
    
    /* Read value from channel 0 */
    if( !readCh0Light(Ch0) ) {
        return FALSE;
    }

    /* Read value from channel 1 */
    if( !readCh1Light(Ch1) ) {
        return FALSE;
    }

    *val = floatAmbientToLux(*Ch0, *Ch1);
    return TRUE;
}

BOOL readAmbientLightLux_Long(unsigned long *val)
{
    UINT16 *Ch0;
    UINT16 *Ch1;
    UINT16 a=0;
    UINT16 b=0;
    
    Ch0=&a;
    Ch1=&b;
    
    /* Read value from channel 0 */
    if( !readCh0Light(Ch0) ) {
        return FALSE;
    }

    /* Read value from channel 1 */
    if( !readCh1Light(Ch1) ) {
        return FALSE;
    }

    *val = ulongAmbientToLux(*Ch0, *Ch1);
     return TRUE;
}

float floatAmbientToLux(UINT16 Ch0, UINT16 Ch1)
{
	UINT8 x[4]={1,8,16,120};
    float ALSIT = 2.73 * (256 - DEFAULT_ATIME);
    float iac  = 1;//max(Ch0 - ALS_B * Ch1, ALS_C * Ch0 - ALS_D * Ch1);
    float lpc;
    if (iac < 0) iac = 0;
	lpc = GA * DF / (ALSIT * x[getAmbientLightGain()]);
    return iac * lpc;
}

unsigned long ulongAmbientToLux(UINT16 Ch0, UINT16 Ch1)
{
	UINT8 x[4]={1,8,16,120};
    unsigned long ALSIT = 2.73 * (256 - DEFAULT_ATIME);
    unsigned long iac  =1;// max(Ch0 - ALS_B * Ch1, ALS_C * Ch0 - ALS_D * Ch1);
    unsigned long lpc;
	if (iac < 0) iac = 0;
    lpc  = GA * DF / (ALSIT * x[getAmbientLightGain()]);
    return iac * lpc;
}

BOOL readCh0Light(UINT16 *val)
{
    UINT8 *val_byte;
    UINT8 a=0;
    val_byte= &a;
    *val = 0;
    
    /* Read value from channel 0 */
    if( !wireReadDataByte(APDS9930_Ch0DATAL, val_byte) ) {
        return FALSE;
    }
    *val = *val_byte;
    if( !wireReadDataByte(APDS9930_Ch0DATAH, val_byte) ) {
        return FALSE;
    }
    *val += (((UINT16)(*val_byte)) << 8);
    
    return TRUE;
}

BOOL readCh1Light(UINT16 *val)
{
    UINT8 *val_byte;
    UINT8 a=0;
    val_byte= &a;
    *val = 0;
    
    /* Read value from channel 0 */
    if( !wireReadDataByte(APDS9930_Ch1DATAL, val_byte) ) {
        return FALSE;
    }
    *val = *val_byte;
    if( !wireReadDataByte(APDS9930_Ch1DATAH, val_byte) ) {
        return FALSE;
    }
    *val += (((UINT16)(*val_byte)) << 8);
    
    return TRUE;
}

/*******************************************************************************
 * Proximity sensor controls
 ******************************************************************************/

/**
 * @brief Reads the proximity level as an 8-bit value
 *
 * @param[out] val value of the proximity sensor.
 * @return TRUE if operation successful. FALSE otherwise.
 */
BOOL readProximity(UINT16 *val)
{    
    UINT8 *val_byte; 
    UINT8 a=0;
    val_byte= &a;
    *val = 0;
    
    /* Read value from proximity data register */
    if( !wireReadDataByte(APDS9930_PDATAL, val_byte) ) {
        return FALSE;
    }
    *val = *val_byte;
    if( !wireReadDataByte(APDS9930_PDATAH, val_byte) ) {
        return FALSE;
    }
    *val += (((UINT16)(*val_byte)) << 8);
    
    return TRUE;
}

/*******************************************************************************
 * Getters and setters for register values
 ******************************************************************************/

/**
 * @brief Returns the lower threshold for proximity detection
 *
 * @return lower threshold
 */
UINT16 getProximityIntLowThreshold()
{
    UINT16 val;
    UINT8 *val_byte;
    UINT8 a=0;
    val_byte= &a;
    
    /* Read value from PILT register */
    if( !wireReadDataByte(APDS9930_PILTL, val_byte) ) {
        val = 0;
    }
    val = *val_byte;
    if( !wireReadDataByte(APDS9930_PILTH, val_byte) ) {
        val = 0;
    }
    val |= (((UINT16)(*val_byte)) << 8);    
    
    return val;
}

/**
 * @brief Sets the lower threshold for proximity detection
 *
 * @param[in] threshold the lower proximity threshold
 * @return TRUE if operation successful. FALSE otherwise.
 */
BOOL setProximityIntLowThreshold(UINT16 threshold)
{
    UINT8 lo;
    UINT8 hi;
    hi = threshold >> 8;
    lo = threshold & 0x00FF;

    if( !wireWriteDataByte(APDS9930_PILTL, lo) ) {
        return FALSE;
    }
    if( !wireWriteDataByte(APDS9930_PILTH, hi) ) {
        return FALSE;
    }
    
    return TRUE;
}

/**
 * @brief Returns the high threshold for proximity detection
 *
 * @return high threshold
 */
UINT16 getProximityIntHighThreshold()
{
    UINT16 val;
    UINT8 *val_byte;
    UINT8 a=0;
    val_byte= &a;
    
    /* Read value from PILT register */
    if( !wireReadDataByte(APDS9930_PIHTL, val_byte) ) {
        val = 0;
    }
    val = *val_byte;
    if( !wireReadDataByte(APDS9930_PIHTH, val_byte) ) {
        val = 0;
    }
    val |= (((UINT16)(*val_byte)) << 8);    
    
    return val;
}

/**
 * @brief Sets the high threshold for proximity detection
 *
 * @param[in] threshold the high proximity threshold
 * @return TRUE if operation successful. FALSE otherwise.
 */
BOOL setProximityIntHighThreshold(UINT16 threshold)
{
    UINT8 lo;
    UINT8 hi;
    hi = threshold >> 8;
    lo = threshold & 0x00FF;

    if( !wireWriteDataByte(APDS9930_PIHTL, lo) ) {
        return FALSE;
    }
    if( !wireWriteDataByte(APDS9930_PIHTH, hi) ) {
        return FALSE;
    }
    
    return TRUE;
}

/**
 * @brief Returns LED drive strength for proximity and ALS
 *
 * Value    LED Current
 *   0        100 mA
 *   1         50 mA
 *   2         25 mA
 *   3         12.5 mA
 *
 * @return the value of the LED drive strength. 0xFF on failure.
 */
UINT8 getLEDDrive()
{
    UINT8 *val;
    UINT8 a=0;
    val= &a;
    
    /* Read value from CONTROL register */
    if( !wireReadDataByte(APDS9930_CONTROL, val) ) {
        return ERROR;
    }
    
    /* Shift and mask out LED drive bits */
    *val = (*val >> 6) & 0b00000011;
    
    return *val;
}

/**
 * @brief Sets the LED drive strength for proximity and ALS
 *
 * Value    LED Current
 *   0        100 mA
 *   1         50 mA
 *   2         25 mA
 *   3         12.5 mA
 *
 * @param[in] drive the value (0-3) for the LED drive strength
 * @return TRUE if operation successful. FALSE otherwise.
 */
BOOL setLEDDrive(UINT8 drive)
{
    UINT8 *val;
    UINT8 a=0;
    val= &a;
    
    /* Read value from CONTROL register */
    if( !wireReadDataByte(APDS9930_CONTROL, val) ) {
        return FALSE;
    }
    
    /* Set bits in register to given value */
    drive &= 0b00000011;
    drive = drive << 6;
    *val &= 0b00111111;
    *val |= drive;
    
    /* Write register value back into CONTROL register */
    if( !wireWriteDataByte(APDS9930_CONTROL, *val) ) {
        return FALSE;
    }
    
    return TRUE;
}

/**
 * @brief Returns receiver gain for proximity detection
 *
 * Value    Gain
 *   0       1x
 *   1       2x
 *   2       4x
 *   3       8x
 *
 * @return the value of the proximity gain. 0xFF on failure.
 */
UINT8 getProximityGain()
{
    UINT8 *val;
    UINT8 a=0;
    val= &a;
    
    /* Read value from CONTROL register */
    if( !wireReadDataByte(APDS9930_CONTROL, val) ) {
        return ERROR;
    }
    
    /* Shift and mask out PDRIVE bits */
    *val = (*val >> 2) & 0b00000011;
    
    return *val;
}

/**
 * @brief Sets the receiver gain for proximity detection
 *
 * Value    Gain
 *   0       1x
 *   1       2x
 *   2       4x
 *   3       8x
 *
 * @param[in] drive the value (0-3) for the gain
 * @return TRUE if operation successful. FALSE otherwise.
 */
BOOL setProximityGain(UINT8 drive)
{
    UINT8 *val;
    UINT8 a=0;
    val= &a;
    
    /* Read value from CONTROL register */
    if( !wireReadDataByte(APDS9930_CONTROL, val) ) {
        return FALSE;
    }
    
    /* Set bits in register to given value */
    drive &= 0b00000011;
    drive = drive << 2;
    *val &= 0b11110011;
    *val |= drive;
    
    /* Write register value back into CONTROL register */
    if( !wireWriteDataByte(APDS9930_CONTROL, *val) ) {
        return FALSE;
    }
    
    return TRUE;
}

/**
 * @brief Returns the proximity diode
 *
 * Value    Diode selection
 *   0       Reserved
 *   1       Reserved
 *   2       Use Ch1 diode
 *   3       Reserved
 *
 * @return the selected diode. 0xFF on failure.
 */
UINT8 getProximityDiode()
{
    UINT8 *val;
    UINT8 a=0;
    val= &a;
    
    /* Read value from CONTROL register */
    if( !wireReadDataByte(APDS9930_CONTROL, val) ) {
        return ERROR;
    }
    
    /* Shift and mask out PDRIVE bits */
    *val = (*val >> 4) & 0b00000011;
    
    return *val;
}

/**
 * @brief Selects the proximity diode
 *
 * Value    Diode selection
 *   0       Reserved
 *   1       Reserved
 *   2       Use Ch1 diode
 *   3       Reserved
 *
 * @param[in] drive the value (0-3) for the diode
 * @return TRUE if operation successful. FALSE otherwise.
 */
BOOL setProximityDiode(UINT8 drive)
{
    UINT8 *val;
    UINT8 a=0;
    val= &a;
    
    /* Read value from CONTROL register */
    if( !wireReadDataByte(APDS9930_CONTROL, val) ) {
        return FALSE;
    }
    
    /* Set bits in register to given value */
    drive &= 0b00000011;
    drive = drive << 4;
    *val &= 0b11001111;
    *val |= drive;
    
    /* Write register value back into CONTROL register */
    if( !wireWriteDataByte(APDS9930_CONTROL, *val) ) {
        return FALSE;
    }
    
    return TRUE;
}

/**
 * @brief Returns receiver gain for the ambient light sensor (ALS)
 *
 * Value    Gain
 *   0        1x
 *   1        4x
 *   2       16x
 *   3      120x
 *
 * @return the value of the ALS gain. 0xFF on failure.
 */
UINT8 getAmbientLightGain()
{
    UINT8 *val;
    UINT8 a=0;
    val= &a;
    
    /* Read value from CONTROL register */
    if( !wireReadDataByte(APDS9930_CONTROL, val) ) {
        return ERROR;
    }
    
    /* Shift and mask out ADRIVE bits */
    *val &= 0b00000011;
	
    return *val;
}

/**
 * @brief Sets the receiver gain for the ambient light sensor (ALS)
 *
 * Value    Gain
 *   0        1x
 *   1        4x
 *   2       16x
 *   3       64x
 *
 * @param[in] drive the value (0-3) for the gain
 * @return TRUE if operation successful. FALSE otherwise.
 */
BOOL setAmbientLightGain(UINT8 drive)
{
    UINT8 *val;
    UINT8 a=0;
    val= &a;
    
    /* Read value from CONTROL register */
    if( !wireReadDataByte(APDS9930_CONTROL, val) ) {
        return FALSE;
    }
    
    /* Set bits in register to given value */
    drive &= 0b00000011;
    *val &= 0b11111100;
    *val |= drive;
    
    /* Write register value back into CONTROL register */
    if( !wireWriteDataByte(APDS9930_CONTROL, *val) ) {
        return FALSE;
    }
    
    return TRUE;
}

/**
 * @brief Gets the low threshold for ambient light interrupts
 *
 * @param[out] threshold current low threshold stored on the APDS-9930
 * @return TRUE if operation successful. FALSE otherwise.
 */
BOOL getLightIntLowThreshold(UINT16 *threshold)
{
    UINT8 *val_byte;
    UINT8 a=0;
    val_byte= &a;
    *threshold = 0;
    
    /* Read value from ambient light low threshold, low byte register */
    if( !wireReadDataByte(APDS9930_AILTL, val_byte) ) {
        return FALSE;
    }
    *threshold = *val_byte;
    
    /* Read value from ambient light low threshold, high byte register */
    if( !wireReadDataByte(APDS9930_AILTH, val_byte) ) {
        return FALSE;
    }
    *threshold = *threshold + (((UINT16)(*val_byte)) << 8);
    
    return TRUE;
}

/**
 * @brief Sets the low threshold for ambient light interrupts
 *
 * @param[in] threshold low threshold value for interrupt to trigger
 * @return TRUE if operation successful. FALSE otherwise.
 */
BOOL setLightIntLowThreshold(UINT16 threshold)
{
    UINT8 val_low;
    UINT8 val_high;
    
    /* Break 16-bit threshold into 2 8-bit values */
    val_low = threshold & 0x00FF;
    val_high = (threshold & 0xFF00) >> 8;
    
    /* Write low byte */
    if( !wireWriteDataByte(APDS9930_AILTL, val_low) ) {
        return FALSE;
    }
    
    /* Write high byte */
    if( !wireWriteDataByte(APDS9930_AILTH, val_high) ) {
        return FALSE;
    }
    
    return TRUE;
}

/**
 * @brief Gets the high threshold for ambient light interrupts
 *
 * @param[out] threshold current low threshold stored on the APDS-9930
 * @return TRUE if operation successful. FALSE otherwise.
 */
BOOL getLightIntHighThreshold(UINT16 *threshold)
{
    UINT8 *val_byte;
    UINT8 a=0;
    val_byte= &a;
    *threshold = 0;
    
    /* Read value from ambient light high threshold, low byte register */
    if( !wireReadDataByte(APDS9930_AIHTL, val_byte) ) {
        return FALSE;
    }
    *threshold = *val_byte;
    
    /* Read value from ambient light high threshold, high byte register */
    if( !wireReadDataByte(APDS9930_AIHTH, val_byte) ) {
        return FALSE;
    }
    *threshold = *threshold + (((UINT16)(*val_byte)) << 8);
    
    return TRUE;
}

/**
 * @brief Sets the high threshold for ambient light interrupts
 *
 * @param[in] threshold high threshold value for interrupt to trigger
 * @return TRUE if operation successful. FALSE otherwise.
 */
BOOL setLightIntHighThreshold(UINT16 threshold)
{
    UINT8 val_low;
    UINT8 val_high;
    
    /* Break 16-bit threshold into 2 8-bit values */
    val_low = threshold & 0x00FF;
    val_high = (threshold & 0xFF00) >> 8;
    
    /* Write low byte */
    if( !wireWriteDataByte(APDS9930_AIHTL, val_low) ) {
        return FALSE;
    }
    
    /* Write high byte */
    if( !wireWriteDataByte(APDS9930_AIHTH, val_high) ) {
        return FALSE;
    }
    
    return TRUE;
}


/**
 * @brief Gets if ambient light interrupts are enabled or not
 *
 * @return 1 if interrupts are enabled, 0 if not. 0xFF on error.
 */
UINT8 getAmbientLightIntEnable()
{
    UINT8 *val;
    UINT8 a=0;
    val= &a;
    
    /* Read value from ENABLE register */
    if( !wireReadDataByte(APDS9930_ENABLE, val) ) {
        return ERROR;
    }
    
    /* Shift and mask out AIEN bit */
    *val = (*val >> 4) & 0b00000001;
    
    return *val;
}

/**
 * @brief Turns ambient light interrupts on or off
 *
 * @param[in] enable 1 to enable interrupts, 0 to turn them off
 * @return TRUE if operation successful. FALSE otherwise.
 */
BOOL setAmbientLightIntEnable(UINT8 enable)
{
    UINT8 *val;
    UINT8 a=0;
    val= &a;
    
    /* Read value from ENABLE register */
    if( !wireReadDataByte(APDS9930_ENABLE, val) ) {
        return FALSE;
    }
    
    /* Set bits in register to given value */
    enable &= 0b00000001;
    enable = enable << 4;
    *val &= 0b11101111;
    *val |= enable;
    
    /* Write register value back into ENABLE register */
    if( !wireWriteDataByte(APDS9930_ENABLE, *val) ) {
        return FALSE;
    }
    
    return TRUE;
}

/**
 * @brief Gets if proximity interrupts are enabled or not
 *
 * @return 1 if interrupts are enabled, 0 if not. 0xFF on error.
 */
UINT8 getProximityIntEnable()
{
    UINT8 *val;
    UINT8 a=0;
    val= &a;
    
    
    /* Read value from ENABLE register */
    if( !wireReadDataByte(APDS9930_ENABLE, val) ) {
        return ERROR;
    }
    
    /* Shift and mask out PIEN bit */
    *val = (*val >> 5) & 0b00000001;
    
    return *val;
}

/**
 * @brief Turns proximity interrupts on or off
 *
 * @param[in] enable 1 to enable interrupts, 0 to turn them off
 * @return TRUE if operation successful. FALSE otherwise.
 */
BOOL setProximityIntEnable(UINT8 enable)
{
    UINT8 *val;
    UINT8 a=0;
    val= &a;
    
    /* Read value from ENABLE register */
    if( !wireReadDataByte(APDS9930_ENABLE, val) ) {
        return FALSE;
    }
    
    /* Set bits in register to given value */
    enable &= 0b00000001;
    enable = enable << 5;
    *val &= 0b11011111;
    *val |= enable;
    
    /* Write register value back into ENABLE register */
    if( !wireWriteDataByte(APDS9930_ENABLE, *val) ) {
        return FALSE;
    }
    
    return TRUE;
}

/**
 * @brief Clears the ambient light interrupt
 *
 * @return TRUE if operation completed successfully. FALSE otherwise.
 */
BOOL clearAmbientLightInt()
{
    if( !wireWriteByte(CLEAR_ALS_INT) ) {
        return FALSE;
    }
    
    return TRUE;
}

/**
 * @brief Clears the proximity interrupt
 *
 * @return TRUE if operation completed successfully. FALSE otherwise.
 */
BOOL clearProximityInt()
{
    if( !wireWriteByte(CLEAR_PROX_INT) ) {
        return FALSE;
    }
    
    return TRUE;
}

/**
 * @brief Clears all interrupts
 *
 * @return TRUE if operation completed successfully. FALSE otherwise.
 */
BOOL clearAllInts()
{
    if( !wireWriteByte(CLEAR_ALL_INTS) ) {
        return FALSE;
    }
    
    return TRUE;
}

/*******************************************************************************
 * Raw I2C Reads and Writes
 ******************************************************************************/

/**
 * @brief Writes a single byte to the I2C device (no register)
 *
 * @param[in] val the 1-byte value to write to the I2C device
 * @return TRUE if successful write operation. FALSE otherwise.
 */
BOOL wireWriteByte(UINT8 val)
{   
    UINT8 writeBuffer[1];
    
    writeBuffer[0] = val;
    
    return Wire_Put(writeBuffer ,1 ,APDS9930_I2C_ADDR );
    
}

/**
 * @brief Writes a single byte to the I2C device and specified register
 *
 * @param[in] reg the register in the I2C device to write to
 * @param[in] val the 1-byte value to write to the I2C device
 * @return TRUE if successful write operation. FALSE otherwise.
 */
BOOL wireWriteDataByte(UINT8 reg, UINT8 val)
{        
    UINT8     writeBuffer[2];
    // build the write buffer first
    // starting address of the EEPROM memory
    
    writeBuffer[0] = (reg | AUTO_INCREMENT);        // high address
    writeBuffer[1] = val;                           // low low address
      
    return Wire_Put(writeBuffer ,2 ,APDS9930_I2C_ADDR );
}

/**
 * @brief Writes a block (array) of bytes to the I2C device and register
 *
 * @param[in] reg the register in the I2C device to write to
 * @param[in] val pointer to the beginning of the data byte array
 * @param[in] len the length (in bytes) of the data to write
 * @return TRUE if successful write operation. FALSE otherwise.
 */
BOOL wireWriteDataBlock( UINT8 reg, UINT8 *val, unsigned int len)
{
    UINT8         writeBuffer[2];
    UINT16        counter, i;
    BOOL            st= TRUE;
   
    for (counter = 0; counter < len; counter++)
    {
        if(st)
        {    
            // build the write buffer first
            // starting address of the EEPROM memory
            writeBuffer[0] = (reg | AUTO_INCREMENT);            // high address
            // data to be written
            writeBuffer[1] = *val++;
            i= 2;
            st--;
                    
        }else
        {
            // data to be written
            writeBuffer[0] = *val++;
            i= 1;
        }
        
        if (!Wire_Put(writeBuffer ,i ,APDS9930_I2C_ADDR ))
            
            return FALSE;
        
    }

    return TRUE;
}

/**
 * @brief Reads a single byte from the I2C device and specified register
 *
 * @param[in] reg the register to read from
 * @param[out] the value returned from the register
 * @return TRUE if successful read operation. FALSE otherwise.
 */
BOOL wireReadDataByte(UINT8 reg, UINT8 *val)
{
    /* Indicate which register we want to read from */
    if (!wireWriteByte(reg | AUTO_INCREMENT)) {
        return FALSE;
    }
        
    /* Read from register */
    if (!Wire_Get(val,1,APDS9930_I2C_ADDR))
        return FALSE;        
    
    return TRUE;
}


/**
 * @brief Reads a block (array) of bytes from the I2C device and register
 *
 * @param[in] reg the register to read from
 * @param[out] val pointer to the beginning of the data
 * @param[in] len number of bytes to read
 * @return Number of bytes read. -1 on read error.
 */
int wireReadDataBlock( UINT8 reg, UINT8 *val, unsigned int len)
{
    UINT16 counter = 0;
    
    /* Indicate which register we want to read from */
    
    for (counter = 0; counter < len; counter++)
    {            
        /* Indicate which register we want to read from */
        if (!wireWriteByte(reg | AUTO_INCREMENT)) {
            return -1;
        }

        /* Read block data */
        if (!Wire_Get(val,1,APDS9930_I2C_ADDR))
            return -1;     
                
        reg++;
        val++;
    }
    return counter;
}