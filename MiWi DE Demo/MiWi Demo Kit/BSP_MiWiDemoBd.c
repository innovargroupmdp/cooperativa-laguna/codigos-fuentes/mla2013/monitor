/*****************************************************************************
 *
 *              BSP_8BitWirelessBd.c -- Hardware Profile
 *
 *****************************************************************************
 * FileName:        BSP_MiWiCardDemoBd.c
 * Dependencies:
 * Processor:       PIC18
 * Compiler:        C18 02.20.00 or higher
 * Linker:          MPLINK 03.40.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * Copyright � 2007-2011 Microchip Technology Inc.  All rights reserved.
 *
 * Microchip licenses to you the right to use, modify, copy and distribute 
 * Software only when embedded on a Microchip microcontroller or digital 
 * signal controller and used with a Microchip radio frequency transceiver, 
 * which are integrated into your product or third party product (pursuant 
 * to the terms in the accompanying license agreement).   
 *
 * You should refer to the license agreement accompanying this Software for 
 * additional information regarding your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY 
 * KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A 
 * PARTICULAR PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE 
 * LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, 
 * CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY 
 * DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO 
 * ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, 
 * LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, 
 * TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT 
 * NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *****************************************************************************
 * File Description:
 *
 *   This file provides configuration and basic hardware functionality 
 *   based on chosen hardware demo boards.
 *
* Change History:
*  Rev   Date         Author    Description
*  1.0   2/02/2011    ccs       Initial revision
********************************************************************/
#include <p18lf26j50.h>

#include "SystemProfile.h"
#include "Compiler.h"
#include "TimeDelay.h"
#include "WirelessProtocols/SymbolTime.h"

#include "HardwareProfile.h"
//#include "User.h"
#include "mcc_generated_files/interrupt_manager.h"  // para las ISR IRQ


#define DEBOUNCE_TIME       0x00001FFF
#define SWITCH_NOT_PRESSED  0
#define SWITCH0_PRESSED     1
#define SWITCH1_PRESSED     2

BOOL switch0Pressed;
MIWI_TICK switch0PressTime;
BOOL switch1Pressed;
MIWI_TICK switch1PressTime;

/*********************************************************************
 * Function:        void BoardInit( void )
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    Board is initialized for P2P usage
 *
 * Overview:        This function configures the board 
 *
 * Note:            This routine needs to be called before the function 
 *                  to initialize stack or any other function that
 *                  operates on the stack
 ********************************************************************/
void BoardInit(void)
{
    INTCONbits.GIEH = 0;
    
    TRISA = 0xFF; //Todos los puertos como entradas
    TRISB = 0xFF;
    TRISC = 0xFF;
    LATA = 0; //Todos los puertos a '0'
    LATB = 0;
    LATC = 0;
    /*******************************************************************/
    // Primary Internal Oscillator
    /*******************************************************************/
    //OSCCON = 0xf8;                                                          // OSC 12 MHZ IDLE MODE SCS<00> internarl o primery CONFIG2L FOSC=
    //--
                                                                
    OSCCONbits.IRCF=7;                                                        // 8Mhz
    
    OSCCONbits.SCS=3;                                                         //seleccion de divisor 
    
    OSCTUNEbits.PLLEN = 0;                                                   
    //OSCTUNEbits.INTSRC= 1;                                                  // OSC SEC 1:intosc/256 0: el de 31khz
    WDTCONbits.SWDTEN = 0;
    
    /*******************************************************************/
	// Configure Sleep tipe of power management
    /*******************************************************************/
    
    DSCONHbits.DSEN=0;                                                   // deep 1 or sleep 0
    DSWAKELbits.DSRTC=1;                                                 // wake-up for rtcc para Deep sleep       
    OSCCONbits.IDLEN=0;                                                  // selecciono el modo 0:sleep o 1:idlen   
    /*******************************************************************/
    // Configure mapping Related Pins
    /*******************************************************************/
    // port tmrclk to other pin
    //EECON2=0x55;                                                              //codigos de seguridad bloqueo registros                         
    //EECON2=0xaa;            
    //PPSCONbits.IOLOCK=0;    
    //***************************
    //*Assign RX2 To Pin RP
    //***************************
    
    //***************************
    //*Assign TX2 To Pin RP
    //***************************
    
    /*******************************************************************/
    // Congifure Secundaty OSC for source RTCC
    /*******************************************************************/		
    T1CONbits.T1OSCEN=1;    //habilita T1OSC driver
                
    /*******************************************************************/
    // Congifure RTCC
    /*******************************************************************/
    RTCCFG=0;
    
    /*******************************************************************/
    // Primary Internal TMR0 se usa para el IEEESeqNum aleatorio 
    /*******************************************************************/		
        
    /*******************************************************************/
    // Primary Internal TMR3 se usa para el MEDIDOR
    /*******************************************************************/
    T3CONbits.TMR3CS=2;        // recordar que hay un pulso de diferencia antes del contador func.
    T3CONbits.T3SYNC=1;        // no sincroniza, aun en sleep cuenta     
    T3CONbits.RD16=1;          // registro de 16bis 
    //**********T3CONbits.T3OSCEN=0;**********************
    T3CON=T3CON & (0b11110111);// apaga el driver del osc1
    T3CONbits.TMR3ON=1;
    
    /*******************************************************************/
    // Configure PPS Related Pins
    /*******************************************************************/
    // Unlock to config PPS
    
    EECON2= 0x55;
    EECON2= 0xAA;
    PPSCONbits.IOLOCK= 0;
    /*inputs*/
    RPINR1= 13;    // seleccion de RP13 como entrada para INT1    
    RPINR21= 2;    // seleccion de RP21 como entrada para SPI2 SDI2
    RPINR1= 13;    // seleccion de RP13 como entrada para INT1       
    /*outputs*/
    RPOR0= 10;     // seleccion de RP0 como salida para SPI2 SCK2
    RPOR1= 9;      // seleccion de RP1 como salida para SPI2 SDO2
    RPINR1= 13;    // seleccion de RP13 como entrada para INT1    
    //RPOR1= 9;      // seleccion de RP13 como salida para SPI2 SDO2
    
    EECON2= 0x55;
    EECON2= 0xAA;
    PPSCONbits.IOLOCK= 1;
    
    /*******************************************************************/
    // AN0 & AN1 Analog Pins others Digital Pins
    /*******************************************************************/
    ANCON0 = 0b11111110;                    // Entrada de bateria AN0 RA0     
    ANCON1 = 0x1F;                          // 
    /*******************************************************************/
    // AN0 Analog Digital Converter bits
    /*******************************************************************/
    ADCON0= 0;                           // ref- and ref+
    ADCON0bits.CHS= 0;                   // AN0
    ADCON1= 0;                           // ADC OFF
    ADCON1bits.ADFM= 1;                  // Justificado Derecho 0000AAAAA    
    ADCON1bits.ADCS= 4;                  // velocidad Fos/64
    ADCON1bits.ACQT= 4;                  // Tacq 20 Tad
    /*******************************************************************/
    // Configure Switch and LED I/O Ports
    /*******************************************************************/
    LED0 = 0;
    LED1 = 0;
    //LED2 = 0;
    //LED3 = 0;
    LED0_TRIS = 0;
    LED1_TRIS = 0;
    //LED2_TRIS = 0;
    //LED3_TRIS = 0;
    
    SW1_TRIS = 1;
    //SW2_TRIS = 1;      
								
    /*******************************************************************/
    // Configure the Medidor Sensor and VBat port
    /*******************************************************************/	
    //bat
    BAT_TRIS = 1; // RA0/AN0/C1INA/ULPWU/RP0
    //medidor
    //Sens_Re_TRIS = 1; // RC2/AN11/CTPLS/RP13        Entrada medidor
    //Sens_En_TRIS = 0; // OSC2/CLKO/RA6             Habilitacion medidor
    /*******************************************************************/
	// Config RF Radio
	/*******************************************************************/

    /*******************************************************************/
	// Config MRF24J40 Pins
	/*******************************************************************/
    INTCON2bits.RBPU = 0;   // Enable PORTB Pull-ups for Switches
    PHY_CS = 1;         // select
    PHY_RESETn = 1;    //reset
    PHY_WAKE = 0;      //wake-up
    
    PHY_CS_TRIS = 0;
    PHY_RESETn_TRIS = 0;        
    PHY_WAKE_TRIS = 0;
    RF_INT_TRIS = 1;
             
    // Config INT0 Edge = Falling
    INTCON2bits.INTEDG0 = 0;
     
    /*******************************************************************/
    // Confiure SPI1
    /*******************************************************************/     
    
    SSP1STAT = 0x40;
    SSP1CON1 = 0x22;
    
    SPI_SDO = 0;        
    SPI_SCK = 0;
    
    SDI_TRIS = 1;
    SDO_TRIS = 0;
    SCK_TRIS = 0;
     
    //SSP1STAT = 0xC0;
    //SSP1CON1 = 0x22;
    PHY_RESETn = 0;
    PHY_RESETn_TRIS = 0;
    PHY_WAKE = 0;      //wake-up
    
    /*******************************************************************/    
    // Configure EEProm Pins
    /*******************************************************************/
    
    /*******************************************************************/
    // Configure RTCC Pins
    /*******************************************************************/
    RTCC_Out_TRIS= 0;           //fines de prueba
    
    /*******************************************************************/
    // Configure SPI2
    /*******************************************************************/   
    
    /*******************************************************************/
    // Enable System Interupts
    /*******************************************************************/
    
    RFIF = 0;
    RFIE = 1;
    INTCONbits.GIEH = 1;

}
/*******************************************************************/
// Interrupciones definidas por el Usuario
/*******************************************************************/
void UserInterruptHandler(void)
{
    if( PIR3bits.SSP2IF )
    {
        PIR3bits.SSP2IF = 0;
        DelayMs(5);
    }   
    
}
