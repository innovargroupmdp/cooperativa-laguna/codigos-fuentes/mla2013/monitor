/* 
 * File:   Proximity.h
 * Author: cba
 *
 * Created on January 7, 2024, 8:51 PM
 */

#ifndef PROXIMITY_H
#define	PROXIMITY_H

#include "Sensor.h"


struct Proximity_t
{ 
  /*Standar API */
  Sensor sensor;
  //Main Measures
  unsigned int proximity;
  /*MIMIA extention*/
  unsigned int  past;
  const Tech *type_t;
  unsigned int id;
};


typedef struct Proximity_t Proximity;


/********************************************************************
 * Instance properties
 ********************************************************************/
BOOL activated(Proximity*);
BOOL hasReading(Proximity*);
unsigned int timestamp(Proximity*);
/********************************************************************
 * Instance methods
 ********************************************************************/
//constructor
void proximity_init(Proximity*, Sense*, unsigned int, Tech* );

void start(Proximity*);
void stop(Proximity*);

void proximity_get(Proximity*);
/********************************************************************
 * Events
 ********************************************************************/
void onActivate(void);
void onError(void);
void onReading(void);

#endif	/* PROXIMITY_H */

