/* 
 * File:   Radiometric.c
 * Author: cba
 *
 * Created on December 15, 2023, 9:57 AM
 */

#include "Radiometric.h"
#include "GenericTypeDefs.h"

/********************************************************************
 * Private definitions
 ********************************************************************/
//int  counter_put(void);
void stop_set(Endstop*);
void stop_clr(Endstop*);
void past_set(Endstop*);
void past_clr(Endstop*);

/********************************************************************
 * Instance properties
 ********************************************************************/
//   Public
BOOL activated(Endstop *this)
{
  Sensor *sensor = &this->sensor;
  return sensor->activated;
}

BOOL hasReading(Endstop *this)
{
  Sensor *sensor = &this->sensor;
  return sensor->hasReading;
}

int timestamp(Endstop *this)
{
  Sensor *sensor = &this->sensor;
  return sensor->timestamp;  
}
//   Private
//static int counter=0;
//static BOOL pulse= FALSE;
//static BOOL started= FALSE;
//static BOOL reading= FALSE;
/********************************************************************
 * Instance methods
 ********************************************************************/
//   Public
void start(Endstop *this)
{
  Sensor *sensor = &this->sensor;
  Sens_En_On();
  sensor->activated = TRUE;
}
void stop(Endstop *this)
{
  Sensor *sensor = &this->sensor;
  Sens_En_Off();
  sensor->activated = FALSE;
}

BOOL pulse_get(Endstop *this)
{
  static BIT a=0;
  Sensor *sensor = &this->sensor;
  /*El sensor tiene salida activa baja */      
  if (a)
  {        
    if (Sens_Re) //Activo bajo ->positivo           
    {   
      a--;       //indica que estubo activo el sensor
      DelayMs(1);
      past_set(sensor);
    }   
  }else 
    if (!Sens_Re) //Activo alto ->negativo (sensor) 
    { 
      //counter++;   //ocurrio
      a++;
      stop_set(sensor);
      past_clr(sensor);
      DelayMs(1);
      return TRUE;
    }
  stop_clr(sensor);
  sensor->hasReading= FALSE;
  sensor->reading= FALSE;
  return FALSE;
}

void endstop_init(Endstop *this, Sense *type_s, unsigned int id, Tech *type_t)
{
  static const SensorVtbl vtbl={
    activated,
    hasReading,
    timestamp,
    start,
    stop,
    onActivate,
    onError,
    onReading
  };
  //Sensor_setInterface(&vtbl);
  Sensor *sensor = &this->sensor;
  
  sensor->vptr= &vtbl;
  sensor->type_s= &type_s;
  sensor->activated= FALSE;
  sensor->hasReading= FALSE;
  sensor->timestamp= 0;
  sensor->activate= FALSE;
  sensor->error= FALSE;
  sensor->reading= FALSE;
  
  this->stop= FALSE;
  this->past= FALSE;
  this->type_t= &type_t;
  this->id= id;
}
//   Private
//int  counter_put(void);
void stop_set(Endstop *this)
{
  Sensor *sensor = &this->sensor;
  sensor->hasReading= TRUE;
  sensor->reading= TRUE;
  this->stop= TRUE;
}
void stop_clr(Endstop *this)
{
  Sensor *sensor = &this->sensor;
  sensor->hasReading= FALSE;
  sensor->reading= FALSE;
  this->stop= FALSE;
}
void past_set(Endstop *this)
{
  this->past= TRUE;
}
void past_clr(Endstop *this)
{
  this->past= FALSE;
}
/********************************************************************
 * Events
 ********************************************************************/
/*
void onActivate(void)
{
  
}
void onError(void)
{
  
}
void onReading(void)
{
  
}
 */ 
