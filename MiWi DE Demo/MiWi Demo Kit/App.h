/* 
 * File:   App.h
 * Author: cba
 *
 * Created on February 13, 2018, 4:04 PM
 */

/* APP_H */

/* *****************************************************************
 * + 19 resto de los campos => MAXIMO en 
 * ConfigApp TX_BUFFER_SIZE 40 RX_BUFFER_SIZE 40
 * PAYLOAD total = LengMesure + (resto campos) 19 = 43
 * *****************************************************************/
#define LengMesure 24 //medidas tomadas durante el dia
#define CntrlLength 10
#define LMmuno     LengMesure-1
#define LMmdos     LMmuno-1
struct DATE_CLK_t_Def
    {
        BYTE Seg;
        BYTE Min;
        BYTE Hou;
        BYTE Day;
        BYTE Mon;
        BYTE Yea;
    };

struct DATE_M_t_Def
    {
        BYTE Hou;
        BYTE Day;
        BYTE Mon;        
    };    
    
struct MED_t_Def
    {
     struct  DATE_M_t_Def  DateMesu;
                 WORD      Mesure;          
    };
    
struct MED_DIF_t_Def
    {    
        BYTE   Index;                      // apunta ultima medida disponible 
        BYTE   MedidaD[LengMesure];
    };    
struct DATCON_t_Def
    {    
        BYTE   Index;                    // apunta ultima medida disponible [0-(Tomas-1)]
        BYTE   Data[CntrlLength];
    };    
    
struct APP_t_Def
    {                   /**************************************-HEADER-********/
                        WORD   ID;
                        WORD   TOKEN;
                        BYTE   T;
                        BYTE   Sec;
                        /**************************************-DATA OF LAYER-*/
                        WORD   Slot;
                        BYTE   N;          //numero de intentos
                        BYTE   Pot;
                        BYTE   Noise;
                        BYTE   Singnal;
                        BYTE   Bat;
                        
     struct   DATE_CLK_t_Def   DateTx;     //para sincronismo completo al llegar los datos tiene la actualizacion del clock
     struct   MED_t_Def        MedidaRE;
     struct   MED_DIF_t_Def    MedidaDif;
                        WORD   SlotDif;
                        BYTE   SubTipo;    //Valido para tramas de control para capa aplicacion   
     struct   DATCON_t_Def     DatCtrl;    //Datos provecientes de Tramas de control   
    };
    
/*Defino nombres para las estructuras de tipos
 * parao poder realizar instancias facilmente*/
typedef struct MED_DIF_t_Def   MED_DIF_t;
typedef struct MED_t_Def       MED_t;
typedef struct DATE_M_t_Def    DATE_M_t;
typedef struct DATE_CLK_t_Def  DATE_CLK_t;
typedef struct APP_t_Def       APP_t;  //Defino nombre App_t para poder realizar instancias facilmente  la declaro en App.h
typedef struct DATCON_t_Def    DATCON_t; 


extern DATCON_t Ctrl_Q;//buffer Recepcion QUERY tramas TIPO Control gral

extern DATCON_t Ctrl_R;//buffer Transmision RESPONStramas TIPO Control gral

extern  APP_t App;
