/* 
 * File:   Sense.c
 * Author: cba
 *
 * Created on December 15, 2023, 9:57 AM
 */
#include "Sensor.h"

//static SensorVtbl **const this = (SensorVtbl *)0;

/********************************************************************
 * Instance properties
 ********************************************************************/
/**
  Sensor.activated Read only

  @Summary:
  Returns a boolean value indicating whether the sensor is active.

  @param:

  @return:
*/
BOOL Sensor_activated(Sensor *const this)
{
  const SensorVtbl *vptr = this->vptr;
  return (*vptr->activated)();
}
/**
  Sensor.hasReading Read only

  @Summary:
    Returns a boolean value indicating whether the sensor has a reading.

  @param:

  @return: bool
*/
BOOL Sensor_hasReading(Sensor *const this)
{
  const SensorVtbl *vptr = this->vptr;
  return (*vptr->hasReading)();
}

/**
  Sensor.hasReading Read only

  @Summary:
    Returns the timestamp of the latest sensor reading.

  @param:

  @return: bool
*/
void Sensor_timestamp(Sensor *const this)
{
  const SensorVtbl *vptr = this->vptr;
  (*vptr->timestamp)();
}


/********************************************************************
 * Instance methods
 ********************************************************************/
/**
  Sensor.start Read only

  @Summary:
    Activates one of the sensors based on Sensor

  @param:

  @return:
*/
void Sensor_start(Sensor *const this)
{
  const SensorVtbl *vptr = this->vptr;
  (*vptr->start)();
}

/**
  Sensor.stop Read only

  @Summary:
    Deactivates one of the sensors based on Sensor.

  @param:

  @return:
*/
void Sensor_stop(Sensor *const this)
{
  const SensorVtbl *vptr = this->vptr;
  (*vptr->stop)();
}

//void Sensor_setInterface(Sensor *interface)
//{
//  vtbl=interface;
//  const SensorVtbl *vptr = this->vptr; 
//  return (*vptr->activated)();
//}

/********************************************************************
 * Events
 ********************************************************************/
/**
  Sensor.onactivate Read only

  @Summary:
    Fired when a sensor becomes activated.

  @param:

  @return:
*/
void Sensor_onActivate(Sensor *const this)
{
  const SensorVtbl *vptr = this->vptr;
  (*vptr->error)();
}

/**
  Sensor.onerror Read only

  @Summary:
    Fired when an exception occurs on a sensor.

  @param:

  @return:
*/
void Sensor_onError(Sensor *const this)
{
  const SensorVtbl *vptr = this->vptr;
  (*vptr->reading)();
}
    
/**
  Sensor.onreading Read only

  @Summary:
    Fired when a new reading is available on a sensor.

  @param:

  @return:
*/
void Sensor_onReading(Sensor *const this)
{
  const SensorVtbl *vptr = this->vptr;
  (*vptr->reading)();
}


void Set_int_Sense(void)
{
  
}

void control_Sense(void)
{
  
}

void SensState(void){
    
    static BIT a=0; 
    /*El sensor tiene salida activa baja */
    /*    
    if (a) 
    {        
        if (Sens_Re)   //Activo bajo Negativo            
        {   
            a--;       //indica que estubo ativo el sensor   
            DelayMs(1);
        }   
    }else 
        if (!Sens_Re) //Activo bajo positvo (sensor) 
        { 
            Counter++;   //ocurrio
            a++;
            DelayMs(1);
        }        
    */
    return;
}

void FunSleep(void)
{
  //SensState();                                                                // duermo el timer antes que el sensor  
  //Delay10us(1);
  //Sens_En_Off();                                                              //duermo el sensor para que el flanco no afecte el TMR1
  
}