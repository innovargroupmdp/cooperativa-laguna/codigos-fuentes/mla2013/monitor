/* 
 * File:   AmbientLight.c
 * Author: cba
 *
 * Created on December 15, 2023, 9:57 AM
 */

#include "AmbientLight.h"
#include "GenericTypeDefs.h"

/********************************************************************
 * Drivers includes
 ********************************************************************/
#include "APDS/APDS9930.h"

/********************************************************************
 * Private definitions
 ********************************************************************/
void luminace_set(Endstop*);
void luminace_clr(Endstop*);
/********************************************************************
 * Instance properties
 ********************************************************************/
BOOL activated(Ambientlight *this)
{
  Sensor *sensor = &this->sensor;
  return sensor->activated;
}

BOOL hasReading(Ambientlight *this)
{
  Sensor *sensor = &this->sensor;
  return sensor->hasReading;
}

int timestamp(Ambientlight *this)
{
  Sensor *sensor = &this->sensor;
  return sensor->timestamp;
}
/********************************************************************
 * Instance methods
 ********************************************************************/
//   Public
void start(Ambientlight *this)
{
  Sensor *sensor = &this->sensor;
  
  if (!enablePower())
  {
    sensor->error= TRUE;
  }
  sensor->error= FALSE;
  sensor->activated = TRUE;
}
void stop(Ambientlight *this)
{
  Sensor *sensor = &this->sensor;
  
  if (!disablePower())
  {
    sensor->error= TRUE;
  }
  sensor->error= FALSE;
  sensor->activated = FALSE;
}

void luminance_get(Ambientlight *this)
{  
  Sensor *sensor = &this->sensor;
  
  this->past=this->luminance;
  if (!readAmbientLightLux_Long(unsigned long &this->luminance))
  {
    sensor->error= TRUE;
  }
  sensor->error= FALSE;
}
void ambientLigth_init(Ambientlight *this, Sense *type_s, int id, Tech *type_t )
{
  static const SensorVtbl vtbl={
    activated,
    hasReading,
    timestamp,
    start,
    stop,
    onActivate,
    onError,
    onReading
  };
  //Sensor_setInterface(&vtbl);
  Sensor *sensor = &this->sensor;
  
  sensor->vptr= &vtbl;
  sensor->type_s= &type_s;
  sensor->activated= FALSE;
  sensor->hasReading= FALSE;
  sensor->timestamp= 0;
  sensor->activate= FALSE;
  sensor->error= FALSE;
  sensor->reading= FALSE;
  
  this->luminance= 0;
  this->past= 0;
  this->type_t= &type_t;
  this->id= id;
}
//   Private

/********************************************************************
 * Events
 ********************************************************************/
/*
  void onActivate(void);
  void onError(void);
  void onReading(void);
 */


