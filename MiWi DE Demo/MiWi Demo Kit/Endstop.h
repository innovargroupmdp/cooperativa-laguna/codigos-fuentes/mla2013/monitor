/* 
 * File:   Endstop.h
 * Author: cba
 *
 * Created on December 22, 2023, 4:06 PM
 */

#ifndef ENDSTOP_H
#define	ENDSTOP_H

#include "Sensor.h"

typedef struct Endstop Endstop;

struct Endstop
{
  /*Standar API*/
  Sensor sensor;
  //Main Measures
  BOOL  stop;
  /*MIMIA extention*/
  BOOL  past;
  const Tech *type_t;
  int id;
};

/********************************************************************
 * Instance properties
 *******************************************************************/
BOOL activated(Endstop);
BOOL hasReading(Endstop);
int  timestamp(Endstop);
/********************************************************************
 * Instance methods
 ********************************************************************/
void start(void);
void stop(void);
void Endstop_init(Endstop*, Sense*, int, Tech* );
/********************************************************************
 * Events
 ********************************************************************/
void onActivate(void);
void onError(void);
void onReading(void);

#endif	/* ENDSTOP_H */

