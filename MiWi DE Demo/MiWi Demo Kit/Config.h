/* 
 * File:   Config.h
 * Author: cba
 *
 * Created on 16 de octubre de 2019, 22:35
 */

#ifndef CONFIG_H
#define	CONFIG_H

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* CONFIG_H */

// Config Bit Settings to get 16 MHz: Internal 8 MHz / 2 = 4 * 12 = 48 / 3 = 16
#pragma config OSC = INTOSC, WDTEN = OFF, XINST = ON, WDTPS = 2048, PLLDIV = 2, CPUDIV = OSC4_PLL6

// CONFIG1L
// #pragma config nDEBUG = 0  
// #pragma config WDTEN = OFF      // Watchdog Timer (Disabled - Controlled by SWDTEN bit)
// #pragma config PLLDIV = 2       // PLL Prescaler Selection bits (Divide by 2 (8 MHz oscillator input))
#pragma config STVREN = OFF      // Stack Overflow/Underflow Reset  (Enabled)
// #pragma config XINST = ON       // Extended Instruction Set (Enabled)

// CONFIG1H
//#pragma config CPUDIV = OSC3_PLL3// CPU System Clock Postscaler (CPU system clock divide by 3)
//-#pragma config CP0 = OFF        // Code Protect (Program memory is not code-protected)

// CONFIG2L
// #pragma config OSC = INTOSCPLL  // Oscillator (INTOSCPLL)
//-#pragma config T1DIG = ON       // T1OSCEN Enforcement (Secondary Oscillator clock source may be selected)
//-#pragma config LPT1OSC = OFF    // Low-Power Timer1 Oscillator (High-power operation)
//- #pragma config FCMEN = ON       // Fail-Safe Clock Monitor (Enabled)
//- #pragma config IESO = ON        // Internal External Oscillator Switch Over Mode (Enabled)

// CONFIG2H
// #pragma config WDTPS = 2048     // Watchdog Postscaler (1:2048)

// CONFIG3L
//- #pragma config DSWDTOSC = INTOSCREF// DSWDT Clock Select (DSWDT uses INTRC)
#pragma config RTCOSC = T1OSCREF    // RTCC Clock Select (RTCC uses T1OSC/T1CKI)
//- #pragma config DSBOREN = ON     // Deep Sleep BOR (Enabled)
//- #pragma config DSWDTEN = ON     // Deep Sleep Watchdog Timer (Enabled)
//- #pragma config DSWDTPS = G2     // Deep Sleep Watchdog Postscaler (1:2,147,483,648 (25.7 days))

// CONFIG3H
//-  #pragma config IOL1WAY = ON     // IOLOCK One-Way Set Enable bit (The IOLOCK bit (PPSCON<0>) can be set once)
//-  #pragma config MSSP7B_EN = MSK7 // MSSP address masking (7 Bit address masking mode)


