/****************************************************************************
* FileName:		Demo.c
* Dependencies: none   
* Processor:	PIC18F26J50	
* Complier:     Microchip C18 v3.04 or higher
* Company:		Microchip Technology, Inc.
*
* Copyright and Disclaimer Notice for P2P Software:
*
* Copyright � 2007-2010 Microchip Technology Inc.  All rights reserved.
*
* Microchip licenses to you the right to use, modify, copy and distribute 
* Software only when embedded on a Microchip microcontroller or digital 
* signal controller and used with a Microchip radio frequency transceiver, 
* which are integrated into your product or third party product (pursuant 
* to the terms in the accompanying license agreement).   
*
* You should refer to the license agreement accompanying this Software for 
* additional information regarding your rights and obligations.
*
* SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY 
* KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY 
* WARRANTY OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A 
* PARTICULAR PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE 
* LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, 
* CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY 
* DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO 
* ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, 
* LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, 
* TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT 
* NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*
****************************************************************************
* File Description:
*
* Change History:
*  Rev   Date         Author    Description
*  1.0   2/02/2011    ccs       Initial revision
*  1.1   7/08/2011    mynenis   Added multi channel support
*  1.2   7/12/2011    mynenis   Range Demo Retry Fix
*  1.3   2/28/2013    mynenis   Fixed the addressing issue in non IEEE 802.15.4 and reduced the
*                               latecy caused by LCD Display
*************************************************************************/

#include "SymbolTime.h"


/************************ HEADERS ****************************************/
#include <p18lf26j50.h>
#include "Config.h"
#include "ConfigApp.h"
#include "HardwareProfile.h"
#include "WirelessProtocols/MSPI.h"
#include "WirelessProtocols/MCHP_API.h"
#include "WirelessProtocols/SymbolTime.h"
#include "TimeDelay.h"
#include "MAC_EEProm.h"


/************************ HEADERS agregados por mi*************************/

#include "App.h"
#include "User.h"
#include "mcc_generated_files/mcc.h"
/************************ HEADERS ****************************************/



// Demo Version
#define MAJOR_REV       1
#define MINOR_REV       3

#define MiWi_CHANNEL        0x04000000                          //Channel 26 bitmap

#define EXIT_DEMO           1
#define RANGE_DEMO          2
#define TEMP_DEMO           3
#define IDENTIFY_MODE       4
#define EXIT_IDENTIFY_MODE  5

#define NODE_INFO_INTERVAL  5


/************************ Definidas por mi ****************************************/
/*** siempre voy a enviar indirectamente o no tramas al PANCO ***********************/
#define PANCO_0 0x00
#define PANCO_1 0x00
 
/****************************************************************/   
/************************ Variables definidas por mi ****************************************/
     
#define SHORTADDRESS_LENGTH  2
#define TIME_SCN             6                     
#define ASOCIA               0x02
#define NORMAL               0x01    
#define POWER_LIMIT          2
#define GAP_ASOCI            8
#define GAP_TX               3
#define N_TRIES              3

typedef enum devState_t_def
{
    devPOR,
    devWaitJoin,
    devAwake,    
            
}devState_t;

typedef enum StateCom_t_def
{
    Norm,
    Diff,
            
}StateCom_t;


typedef enum MedState_t_def
{
    MedPOR,
    MedInit,
    MedAwake,
    MedSleep,
            
}MedState_t;

/*************************************************************************/
// The variable myChannel defines the channel that the device
// is operate on. This variable will be only effective if energy scan
// (ENABLE_ED_SCAN) is not turned on. Once the energy scan is turned
// on, the operating channel will be one of the channels available with
// least amount of energy (or noise).
/*************************************************************************/


extern BYTE myLongAddress[]; //definida en MiWI.c

// Possible channel numbers are from 0 to 31

rom BYTE myChannel = 24;

//BYTE ConnectionEntry = 0;
			
//BOOL NetFreezerEnable = FALSE;

/*************************************************************************/
// AdditionalNodeID variable array defines the additional 
// information to identify a device on a PAN. This array
// will be transmitted when initiate the connection between 
// the two devices. This  variable array will be stored in 
// the Connection Entry structure of the partner device. The 
// size of this array is ADDITIONAL_NODE_ID_SIZE, defined in 
// ConfigApp.h.
// In this demo, this variable array is set to be empty.
/*************************************************************************/
#pragma udata  bank1=0x100

    #if ADDITIONAL_NODE_ID_SIZE > 0
        BYTE AdditionalNodeID[ADDITIONAL_NODE_ID_SIZE] = {0x00};
    #endif

     typedef struct addr_t_def
    {
        BYTE bytes[SHORTADDRESS_LENGTH];
    }addr_t;

    APP_t App;

    DATCON_t Ctrl_Q;//buffer Recepcion QUERY tramas TIPO Control gral
    DATCON_t Ctrl_R;//buffer Transmision RESPONStramas TIPO Control gral

    BYTE MyAddr[SHORTADDRESS_LENGTH]; // Direccion de NODO variable o alternativa
    BYTE MyID[SHORTADDRESS_LENGTH];   // Direccion de ID dado por el Server 
    BYTE PANCO[SHORTADDRESS_LENGTH] = {PANCO_0,PANCO_1}; // direccion de nodo pan           
    WORD_VAL PANID;

    MIWI_TICK tick1 ,tick2;

    BYTE TxPower;

#pragma udata

/************************ Variables ****************************************/
#pragma idata
    //Estado de conexcion
    devState_t devState=devPOR;                                                     
    MedState_t MedState=MedPOR;     
#pragma idata

/*********** Prototipos de Funciones definidas por mi ****************************************/

BOOL MiApp_IsMemberOfNetwork(void);
BYTE EstablishConnection(void);
BOOL FunRx(BOOL);
BOOL FunTx(void);
BOOL FunCom(void);

/************************ Funciones **********************************/
                                                                                               
/*********************************************************************
* Function:         void main(void)
*
* PreCondition:     none
*
* Input:		    none
*
* Output:		    none
*
* Side Effects:	    none
*
* Overview:		    This is the main function that runs the simple 
*                   example demo. The purpose of this example is to
*                   demonstrate the simple application programming
*                   interface for the MiWi(TM) Development 
*                   Environment. By virtually total of less than 30 
*                   lines of code, we can develop a complete 
*                   application using MiApp interface. The 
*                   application will first try to establish a P2P 
*                   link with another device and then process the 
*                   received information as well as transmit its own 
*                   information.
*                   MiWi(TM) DE also support a set of rich 
*                   features. Example code FeatureExample will
*                   demonstrate how to implement the rich features 
*                   through MiApp programming interfaces.
*
* Note:			    
**********************************************************************/


void main(void)
{   
  BYTE i,j;
	
	BYTE switch_val;
	WORD VBG_Result;
	
  /*******************Definidas por mi************************************************/
  BYTE counter=0;
  BOOL Tx_Ok_Q=TRUE;
  BOOL result = TRUE;         //conviene destruirla solo un uso

  BYTE Status;
  DWORD scan;
  BOOL MedState_PostNext=FALSE;
  BOOL MedState_Prev_Awake=FALSE;
  FunClearGlobal();

  /*******************************************************************/
  /*****************
   * para pruebas
   ****************/
  //App.Slot=1;
    
	//NetFreezerEnable = FALSE;

  /*******************************************************************/
  // Initialize Hardware
  /*******************************************************************/
	BoardInit();
  /*******************Definidas por mi*****************/
  SYSTEM_Initialize();
  /****************************************************************/
  //  Almacena la direccion EUI en  myLongAddress
  /*******************************************************************/
    
 	Read_MAC_Address();//MODIFICAR PARA QUE SE ESCRIBE  DESDE CONFAPP
    
  /*******************************************************************/
  // Initialize the MiWi Protocol Stack. The only input parameter indicates
  // if previous network configuration should be restored.
  /*******************************************************************/		

  MiApp_ProtocolInit(FALSE);

  //SelfTest(myChannel);// deberia enceder un led!!! definida en la demo

  /*******************************************************************/
  // Set-up PAN_ID
  /*******************************************************************/
        
CreateorJoin:

  /*******************************************************************/
  //Ask Use to select channel
  /*******************************************************************/

  //tick1 = MiWi_TickGet();

  //while(MiWi_TickGetDiff(MiWi_TickGet(),tick1) < (ONE_SECOND/32)){

  /***********Pueba de reset por overflow*************************
   * if (STKPTRbits.STKOVF | STKPTRbits.STKUNF)
  {
      LED2=1;
      DelayMs(500);
      LED2=0;

  }
   ************************************************************* /

  /*******************************************************************/
  // Set Device Communication Channel
  /*******************************************************************/

  MiApp_SetChannel(myChannel);

  /*******************************************************************/
  //  Set the connection mode. The possible connection modes are:
  //      ENABLE_ALL_CONN:    Enable all kinds of connection
  //      ENABLE_PREV_CONN:   Only allow connection already exists in 
  //                          connection table
  //      ENABL_ACTIVE_SCAN_RSP:  Allow response to Active scan
  //      DISABLE_ALL_CONN:   Disable all connections. 
  /*******************************************************************/
  MiApp_ConnectionMode(DISABLE_ALL_CONN);

  /*******************************************************************/
  // Ask User to Create or Join a Network
  /*******************************************************************/

  //DelayMs(1000);

  scan=0x0ff00000;        

  /*******************************************************************/
  // Join a Network
  /*******************************************************************/

  //MiApp_ProtocolInit(FALSE);

  //LED2=~LED2;
  //tick1 = MiWi_TickGet();
  //while(MiWi_TickGetDiff(MiWi_TickGet(),tick1)<(ONE_SECOND*1));

  /******************************************************************
  // Function MiApp_EstablishConnection try to establish a new 
  // connection with peer device. 
  // The first parameter is the index to the active scan result, 
  //      which is acquired by discovery process (active scan). If 
  //      the value of the index is 0xFF, try to establish a 
  //      connection with any peer.
  // The second parameter is the mode to establish connection, 
  //      either direct or indirect. Direct mode means connection 
  //      within the radio range; indirect mode means connection 
  //      may or may not in the radio range. 
  // Active scan se encuentra incorporado en establish al setear CONN_MODE_DIRECT
  ******************************************************************/
  TxPower=0;
  do
  { /*Prueba de enlace con tapa abierta*/ 
    MiMAC_SetPower(TxPower);
    TxPower++;
  }while((MiApp_SearchConnection(TIME_SCN +1,scan)<1) && (TxPower<POWER_LIMIT));
  /*Subo potiencia por tapa cerrada*/
  MiMAC_SetPower(TxPower);

  //realiza un barrido y encuentra redes es los canales                
  //scanresult = MiApp_SearchConnection(10, (0x01000000 << (myChannel-24)));

  /*******************************************************************/
  // Establish Connection and Display results of connection
  /*******************************************************************/								    
  //MiApp_EstablishConnection(0xFF, CONN_MODE_DIRECT); // primer alcance por rango  ff aleatorio

  if(EstablishConnection()== 0xFF) 
  {
    /****"Join Failed!!!"*********
     * la tabla del dispositivo estaria llena o error 
    *****************************/
    //MiApp_FlushTx();
    //MiApp_DiscardMessage();            

    goto CreateorJoin;            
  }

  //"Joined  Network Successfully.."
  // CONECTA CON AQUELLA DE MERJOR SE�Al

  /*******************************************************************/
  // If Connected to a Network Successfully bail out
  /*******************************************************************/    	


  /*******************************************************************/
  // Programa Principal
  /*******************************************************************/
    
	while(1){
    	
    //LED0 = LED1 = LED2 = 0;

      //FunChekOSC();                                                           //Oscillator Fail Interrupt Flag bit
      switch(MedState)
      {        
        case MedPOR:
            //Deberia iniciar o tomar estados previos al inicio
            MedState= MedInit;

            FunCom();                                                        //evoluciona state devPOR:

          break;

        case MedInit:

            //Inicializar interrupciones, perifericos,RTCC
            //inicio de Interrupciones deberia entrar una sola vez
            InitInts();

            MedState= MedSleep;              // Mantengo slots de tiempos    

            if(MedState_Prev_Awake & PIR3bits.RTCCIF)                     
            {
                PIR3bits.RTCCIF=0; 
                FunRTCC(RTCC_ON,Tx_Ok_Q,FALSE);  //salvo datos si es requerido
            }                           

            if (FunCom())                   //evoluciona state devWaitJoin
            {                    
                MedState_PostNext= FALSE;    // luego va a MedAwake;
            }    
            else
                MedState_PostNext= TRUE;     // Mantengo en MedInit

            //vereficar si hubo conex con PAN bucle si no la hay hasta que encuentre                
          break;

        case MedAwake:
            // verificacion interrupciones toma mediciones, realiza correciones RTCC, Realiza la comunicacion

            /*********************PROGRAMA GENERAL*************************/
            MedState= MedSleep;
            //consulta interrupcion, de otro modo ubicar en si es 
            //deseado drv_mrf_miwi_mesh.c en mla2017 en MLA2013 va en UserInterruptHandler en BSP_MIWIDemoBd.c
            if (PIR3bits.RTCCIF)                                                
            {
              PIR3bits.RTCCIF=0;

              if (FunRTCC( RTCC_ON, Tx_Ok_Q, FALSE))  //Segun el tiempo envio datos por ranura
              {                          
                if(FunCom())//Indica si pudo subir los datos de lo contrario difiere
                {
                  Tx_Ok_Q= TRUE;//tramas NORm
                  /*Ajusto RTCC y en el slot actual de cuenta*/
                  FunRTCC(RTCC_FIX, Tx_Ok_Q, TRUE);                           
                }
                else
                {    
                  if(Tx_Ok_Q) //caso de venir de Tx Norm y FunCom Error   
                  { 
                   /*  La maquina de estados FunCom se 
                    *  encuentra apuntando a:
                    *  devState=devAwake 
                    *  Vuelve a intentar comunicar pero con DIF.*/                                 
                      Tx_Ok_Q= FALSE;//Cambia el deferimiento consulta en FunRTCC()                               
                  }
                  else
                  { //caso de venir de Tx DIF y FunCom Error
                   /* La tx aqui sigue siendo erronea
                    * con envios diferidos y acumulacion de medidas
                    * La maquina de estados FunCom se encuentra apuntando a:
                    * devState=devWaitJoin
                    * Requiere comprobar si la conexion es valida */
                     Tx_Ok_Q= TRUE;            //Cambia Tx Normal consulta en FunRTCC()
                     MedState_PostNext= TRUE;  //MedInit;
                     MedState_Prev_Awake= TRUE;//Indico que llego a despertar
                  }
                }    
              }
            }

          break;    

        case MedSleep:
            //Ajusta interrupciones,coloco intosc en osclow y Paso modo sleep
            //permanezco en este estado hasta los 10 seg de la int del RTC
            //luego de interrupcion 10seg coloco intosc en OSCHigh luego de salir spleep
            //Desactiva INT TMR0 despierta de modo incorrecto al MEdidor

            LED0=0;/*******/
            //FunSleep();  // en caso de corte de energia, el medidor funciona a modo ahorro
            //duerme por 6ms utilizar timer.                                  
            FunSleep_USER();              
            //FunAwake();
            LED0=1;/*******/

            if (MedState_PostNext)
            {
              /*Aseguro tener slot completo para reconexion*/
              MedState= MedInit;                  
            }else
              MedState= MedAwake;

          break;
      }                
	}
}

/*****************************************************************************
 * En base a ActivescanResults
 * SE Vefica que el padre, de no estar disponible encuentra otro
 * PUEDEN ESTAR EN EL MISMO CANAL PERO NO ES EL MISMO PANID
 * DETECTO SI HAY NODO RECOLECTOR CON ALCANCE
 * 
 ****************************************************************************/
BOOL MiApp_IsMemberOfNetwork(void)
{

    BYTE scn,i,j,k;
    
    /********************************************
     * Determino si en el canal se encuentra una red
     *******************************************/
    if(myChannel < 8)
                scn = MiApp_SearchConnection(TIME_SCN +1, (0x00000001 << myChannel));
            else if(myChannel < 16)
                scn = MiApp_SearchConnection(TIME_SCN +1, (0x00000100 << (myChannel-8)));
                else if(myChannel < 24)
                    scn = MiApp_SearchConnection(TIME_SCN +1, (0x00010000 << (myChannel-16)));
                    else 
                        scn = MiApp_SearchConnection(TIME_SCN +1, (0x03800000 << (myChannel-24)));
                                                                                                                                 
    /********************************************
     * Determino si esta presente la red inicial
     *******************************************/
    if(scn > 0)
    {        
        for (i=0;i<scn;i++)
        {          
            if (ActiveScanResults[i].PANID.Val==PANID.Val )                     //valor actual PANid
            {                        
                for (j=0;j<CONNECTION_SIZE;j++)
                {                                                                         
                    if(ConnectionTable[j].status.bits.isValid && ConnectionTable[j].status.bits.shortAddressValid)
                    {
                        tempShortAddress.v[0] = ActiveScanResults[i].Address[0];
                        tempShortAddress.v[1] = ActiveScanResults[i].Address[1];
                        
                        if(ConnectionTable[j].AltAddress.Val == tempShortAddress.Val)
                        
                            return(TRUE);                        
                    }                                        
                }                                                   
            }
        }    
    }
    
    return(FALSE);
}


/*******************************************************************************************
********************algoritmo que permite obtener el enlace de calidad mas alto******************
* #define ROLE_FFD_END_DEVICE 0x00
* #define ROLE_COORDINATOR 0x01
* #define ROLE_PAN_COORDINATOR 0x02
* output : 0xFF if fail, any else if ok
*******************************************************************************************/
#define ROLE 0x01

BYTE EstablishConnection(void){

    BYTE j,k=0,m=0;
    
    
     /*********************************************************
     * Conecto con el dispositivo de mayor calidad de conexion LQIValue 
     ********************************************************/
    
    /*me quedo con aquel con menor error binario**
     * m : tienen mayor LQI
     * k : tiene mayor RSSI
     ********************************************/
    if (ActiveScanResultIndex){
        /*********************
         * indice Calidad de enlace: 
         * inversamente proporcional a la tasa binaria de error
         ********************/
        for (j=0;j<ActiveScanResultIndex;j++)

                if (ActiveScanResults[j].LQIValue>ActiveScanResults[m].LQIValue && ActiveScanResults[j].Capability.bits.Role>=ROLE)
                    m=j;  
        /*******************************************
         * indicador de intensidad de se�al recibida: 
         * indica la distancia, potencia para transmitir
         *******************************************/
        for (j=0;j<ActiveScanResultIndex;j++)

                if (ActiveScanResults[j].RSSIValue>ActiveScanResults[k].RSSIValue && ActiveScanResults[j].Capability.bits.Role>=ROLE)
                    k=j;  
    
    }else        
        return(0xFF); 
    
    if (k==m) {
              
       if( MiApp_EstablishConnection(k, CONN_MODE_DIRECT)!=0xFF){             
            PANID.v[0]=ActiveScanResults[k].PANID.v[0];
            PANID.v[1]=ActiveScanResults[k].PANID.v[1];
            App.Noise=ActiveScanResults[k].LQIValue;
            App.Singnal=ActiveScanResults[k].RSSIValue;
            return(k);
       }
       
    }    
    
    
    if (MiApp_EstablishConnection(k, CONN_MODE_DIRECT)!=0xFF){
        
        PANID.v[0]=ActiveScanResults[k].PANID.v[0];
        PANID.v[1]=ActiveScanResults[k].PANID.v[1];
        
    }else{
        // establece en cualquiera
        k=MiApp_EstablishConnection(0xFF, CONN_MODE_DIRECT);
        
        if(0xFF==k)
        {
            MiApp_FlushTx();
            MiApp_DiscardMessage();
            return(0xFF);
        }
        PANID.v[0]=ConnectionTable[k].PANID.v[0];
        PANID.v[1]=ConnectionTable[k].PANID.v[1];
    }    
    
    App.Noise=ActiveScanResults[k].LQIValue;
    App.Singnal=ActiveScanResults[k].RSSIValue;
    
    
    MiApp_FlushTx();
    MiApp_DiscardMessage();
    
    
    return(k);
}


// indices en payload para RX
#define mUDT    0
#define ID_H    1
#define ID_L    2
#define TK_H    3
#define TK_L    4
#define Tipo    5 
#define Secu    6
#//define Nint    5
#define Slot_H  7
#define Slot_L  8
#define seg     9
#define min     10
#define hh      11
#define DD       12
#define MM       13
#define AA       14
#define ID_AUT_H 15
#define ID_AUT_L 16

#define SubT     7
#define Indx     8
// tipos de trama

/*primerias*/
//      Error    0
//      Normal   1
#define Asoc     2 
#define Time     3

/*secundarias*/
#define Control  4
#define Rst      5
#define Alarm    6
//Red
#define Tx_Power 7
#define Channel  8
#define Slt     9
#define Chng_Red 10
#define Tabla_h  11
#define Tabla_p  12


// tipos de RECEPCION
#define Asoc_RX FALSE  
#define Norm_RX TRUE

BOOL FunRx(BOOL s)
{
    BYTE *p, i;
    WORD A;
    MIWI_TICK tStart;
    BOOL F=TRUE;
    
    p=&App.DateTx.Seg;
    
    App.Noise=rxMessage.PacketLQI;
    App.Singnal=rxMessage.PacketRSSI;           
            
    if(s)
    {                                                   
        /*******Proceso NORMAL primer paquete 90 bytes en total el buffer******/
        switch (rxMessage.Payload[Tipo])
        {
            case OP_NORMAL:
                /******************ACCESS ID and SEC*********************************************/

                if((MyID[0]==rxMessage.Payload[ID_L]) && (MyID[1]==rxMessage.Payload[ID_H]) && 
                        (App.Sec==rxMessage.Payload[Secu]))
                {    
                    App.TOKEN=((0x00ff&rxMessage.Payload[TK_H])<<8)|rxMessage.Payload[TK_L];
                    
                    if((rxMessage.Payload[Slot_H] | rxMessage.Payload[Slot_L] )!=0)
                    {
                        App.Slot=( (0x00ff & rxMessage.Payload[Slot_H])<<8 )|rxMessage.Payload[Slot_L] ;
                    }
                    
                    // desde (tiempo) 
                    for(i=0;i<3;i++)
                    
                        *p++=rxMessage.Payload[i+seg];                                                                                     
                                        
                }else
                    F=FALSE;
                
                break; 
                
            case OP_TIME:
                // @todo Completar frame de time
                
                break;
                
            case OP_CONTROL:
                    App.TOKEN= ((0x00ff&rxMessage.Payload[TK_H])<<8)|rxMessage.Payload[TK_L];
                    App.SubTipo= rxMessage.Payload[SubT];
                    
                    //MiApp_WriteData((BYTE)App1.SubTipo);
                    //MiApp_WriteData((BYTE)App1.DatCtrl.Index);
                    p= &Ctrl_Q.Index;
                    for (i=0;i<=(rxMessage.Payload[Indx]+1);i++)
                        
                        *p++=rxMessage.Payload[i+SubT];//<-(20)
                break;
            default:
                   F=FALSE;;          
        }
      
    }else{      
        /*******************Proceso DE ASOCIACION*******************************/
        // debe coincidir la secuencia y tipo
        if ( App.Sec==rxMessage.Payload[Secu]   && rxMessage.Payload[Tipo]==ASOCIA
            && MyID[0]==rxMessage.Payload[ID_L] && MyID[1]==rxMessage.Payload[ID_H] ) 
        {
            App.TOKEN=((0x00ff&rxMessage.Payload[TK_H])<<8)|rxMessage.Payload[TK_L];
            //Almaceno el Slot
            App.Slot=((0x00ff&rxMessage.Payload[Slot_H])<<8)|rxMessage.Payload[Slot_L];
            
            //almaceno el RTCC            
            for(i=0;i<6;i++) 
            {
                *p++=rxMessage.Payload[i+seg];                              
            }
            //Asigno la ID del Servidor podria se App.ID_AUT
            MyID[1]=rxMessage.Payload[i++];
            MyID[0]=rxMessage.Payload[i];
            
        }else
            F=FALSE;              
    }
    
    MiApp_DiscardMessage();
    return(F);
    
    //MiApp_RequestData();                                    //Keep requesting data, so that all buffered
                                                            //packets are delivered, not just the first one
}


/*******************************************************************************
 * PAYLOAD_START apunta al inicio del buffer
 *******************************************************************************
      Direccion Larga de nod solo primeros 2 bytes  en caso de asociacion
       myLongAddress[0]=0x01;
       myLongAddress[1]=0x00;
     ***************************************************************************
********-HEADER------------------------------
1 (App.ID);      //IDENTIFICADor
2 (App.TOKEN);   //token
3 (App.TIPO);   //tipo de trama
4 (App.Sec);     //Secuencia 
********-DATA OF LAYER------------------------
5 (App.N);       //intentos de transmision    
6 (App.Noise);   //calidad de ambiente
7 (App.Singnal); //calidad de ambiente
8 (App.Bat);     //bateria 
    
 *******************************************************************************/
BOOL FunTx(void)
{    
    BYTE i,C;
    WORD A;
    BYTE *p;
    BOOL b=FALSE;
    MIWI_TICK tStart;
            
    //MiApp_TransceiverPowerState(POWER_STATE_WAKEUP);
    //memcmp (coord.bytes,0x00,sizeof(coord));                      //string.h
                                        
    App.Sec++;                                                      //secuencia de ident                    
    App.N++;
    /*
     //mide bateria y fecha de tx
     */
    FunAlarm();
    MiApp_FlushTx();
    //typo para compatibilidad futura, indica comienzo de datos 
    //usurio ':'si le sumo '0' o LF si es control 
    
    MiApp_WriteData(mUserDataType);                                         
    /*******************************************************-HEADER-*/ 
    MiApp_WriteData((BYTE)MyID[1]);    //MSB
    MiApp_WriteData((BYTE)MyID[0]);    //LSB 
    MiApp_WriteData((BYTE)(App.TOKEN >> 8));
    MiApp_WriteData((BYTE)(App.TOKEN & 0x00FF));
    MiApp_WriteData((BYTE)App.T);
    MiApp_WriteData((BYTE)App.Sec);
    /************************************************-DATA OF LAYER-*/                 
        
    switch (App.T) 
    {    //indica longitud total de payload MB      
        case nOP:
            /*------------TRAMA CORTA UNICO  ENVIO----------------------------*/                                
            /*******************************************************-HEADER-*/
            
            /************************************************-DATA OF LAYER-*/                                
        break;    
        case OP_NORMAL:
                                 
            C=TxPower;
            i=0;
            while(C>9)
            {
                C-=10;
                i+=0x10;
            }                           
            App.Pot=(i|C);//LSB
            p=&App.N;
            /************************************************-DATA OF LAYER-*/            
            MiApp_WriteData((BYTE)((App.Slot&0xFF00)>>8));   //9            
            MiApp_WriteData((BYTE)(App.Slot&0x00FF));        //10                      
            
            for(i=0;i<11;i++) // N,P,n,Rssi,bat,DateTX(s,m,h),dateNed(h,D,m),
                MiApp_WriteData(*p++);
            /*
            MiApp_WriteData(App.DateTx.Seg);    //11        
            MiApp_WriteData(App.DateTx.Min);    //12
            MiApp_WriteData(App.DateTx.Hou);    //13
            
            MiApp_WriteData(App.MedidaRE.DateMesu.Hou); //14
            MiApp_WriteData(App.MedidaRE.DateMesu.Day); //15
            MiApp_WriteData(App.MedidaRE.DateMesu.Mon); //16
            */
            MiApp_WriteData((BYTE)((App.MedidaRE.Mesure&0xFF00)>>8));       //17            
            MiApp_WriteData((BYTE)(App.MedidaRE.Mesure&0x00FF));            //18                  

            //MiApp_WriteData(App.MedidaDif.Index);       //19
            p=&App.MedidaDif.Index;   
            for(i=0;i<=(App.MedidaDif.Index+1) ;i++)                                              // hou,day,mes                    
            {   
                MiApp_WriteData(*p++);
            }
            
        break;  
        
        case OP_ASOCIACION:  
            //Tanto FFD , Monitores   
            //Trama corta permite asociacion y ganar contienda
            /*******************************************************-HEADER-*/
            //PAYLOAD_START Reescribo el buffer con las AUI para autenticar MAC en servidor
            TxBuffer[PAYLOAD_START+1] = myLongAddress[1]; //MSB
            TxBuffer[PAYLOAD_START+2] = myLongAddress[0]; //lSB        
            /************************************************-DATA OF LAYER-*/             
        break;
        
        case OP_TIME:
            
            /*******************************************************-HEADER-*/
            /************************************************-DATA OF LAYER-*/ 
            //proceso primer paquete X bytes en total el buffer            
            
        break;
        
        case OP_ASOCIACION_PAN: //SOLO COOR-PAN
                
                return (FALSE);
             
        break; 
        
        case OP_CONTROL:
            
            /*******************************************************-HEADER-*/
            /************************************************-DATA OF LAYER-*/ 
            
                /*-DATA Of APPLICATION-*/ 
                p=&App.SubTipo;
                //MiApp_WriteData((BYTE)App1.SubTipo);
                //MiApp_WriteData((BYTE)App1.DatCtrl.Index);

                for (i=0;i<=App.DatCtrl.Index+2;i++)
                    MiApp_WriteData(*p++);//<-(20)   
                
                /*
                p= &Ctrl_R.Index;
                    for (i=0;i<=(rxMessage.Payload[Indx]+1);i++)
                        
                        *p++=rxMessage.Payload[i+SubT];//<-(20)
                */
        break; 
        
        default:
            return (FALSE);
    }      
    
    //si resulta en una transmision correcta  
    //Envio al PANCO la trama
    //true direc MAC, FALSE indirect por AltAddress
    
        
    if(MiApp_UnicastAddress(PANCO, FALSE, FALSE))
    {  
        //App.N=0;
        b=TRUE;
    }
        
    return(b);
}

BOOL FunCom(void)
{   
    
    static StateCom_t StateCom=Norm;
    static BOOL       Joined=FALSE;
        
    BOOL SendPacket=TRUE;
    BOOL RequestData=TRUE;
    BOOL StatusAsoc=TRUE;
    BOOL Connect=0;
    BOOL exit0;
    BYTE i;
    int R,R1;
            
    tick1=MiWi_TickGet();
    
    while(MiApp_TransceiverPowerState(POWER_STATE_WAKEUP));
   
    switch(devState)
    {
        case devPOR:
            
            devState=devWaitJoin;
            
          break;

        case devWaitJoin:
            
            /*******************************************************************
             * Verifico si existe red en alcance
             * Verifico si la red es la actual
             * Verifico si el padre se encuentra activo
             * Casos de frontera:
             *     PAN1         ||      PAN2
             * -----------------------------------
             * coor1 |  coor2   || coor1 |  coor2
             * -----------------------------------
             *       |          ||       |
             *       |         caso2     |
             *      caso1       ||     caso1
             ******************************************************************/
            StatusAsoc=FALSE;
            
            
            if(MiApp_IsMemberOfNetwork())
            {
                devState=devAwake;//redundancia
                StatusAsoc=TRUE;  //reduncancia
            }    
            else    
            {
             /******************************************************************
             * inicio una nueva conexion con otro PAN u Otro Padre
             *******************************************************************/
                Connect=EstablishConnection();
                /*De existir baja senal subo potencia
                  para que responda el coor */                
                if((Connect==0xFF) && (TxPower<POWER_LIMIT))
                {
                      MiMAC_SetPower(++TxPower);
                      
                      /*aseguro un nuevo ciclo de busqueda a menor potencia*/
                      if(TxPower==POWER_LIMIT)
                          TxPower=0;
                      
                }
                /*Deberia agregar algo que busque en otros canales*/      
            }

            MiApp_DiscardMessage();                                 // solo con fines de iniciacion Elimina dato de iniciacion de request
            tick1=MiWi_TickGet();      
            /*******************************************************************
             **** Entro aqui si solo requiero autenticar frente al servidor*****
             ******************************************************************/
            if(!Joined)
            {
                App.T=ASOCIA; ///-------------deberia ir asociacion| 1:normal 2:Asociacion 0:Nop
                if(!FunTx())  
                {                   
                    devState=devWaitJoin;                                //envia trama al coordinador para asociar al servidor))
                    StatusAsoc=FALSE;                                    //Si no hay comunicacion debo volver a diferir                                
                }                
                
                while((!Joined)&&(MiWi_TickGetDiff(MiWi_TickGet(),tick1)<(ONE_SECOND/32)*GAP_ASOCI))
                {    
                    /****Espero que haya mensaje del servidor****/    
                    DelayMs(100);
                    if(MiApp_MessageAvailable())                              //verifico si existe respuesta con datos de asociacion.
                    {   
                        if(FunRx(Asoc_RX))                                    //tiene el MiApp_DiscardMessage() dentro
                        {  
                            /*Envio exitoso*/
                            App.N=0x00;
                            App.T=NORMAL;
                            Fun_Rtcc_Fix();                                     //Seteo el RTCC
                            Joined=TRUE;                                        //Se encuentra asociado en el servidor    
                            
                            
                        }
                        else
                        {
                            devState=devWaitJoin;
                            StatusAsoc=FALSE;                        
                        }
                    }
                    else
                    {
                        devState=devWaitJoin;
                        StatusAsoc=FALSE;                                        
                    }
                }
            }
            
            if((Connect!=0xff) && Joined)
            {
                devState=devAwake;
                StatusAsoc=TRUE; 
                
            }
          break;

        case devAwake:  //EL TxRx se encuentra asociado. Si ya envie verifica que existan datos para el
            
            
            App.T=NORMAL;                        
            SendPacket=FALSE;
            RequestData=FALSE;
                                                        
            /*Concepto de diferimiento aleatorio */
            // renera semilla para rand siguiente          
            srand(tick1.word.w0);                        
            R=rand();                   
            while(R>=50) R>>=1;//divido 2
            
            if(TMRL&0x01)
                if (R>(TMRL&0x1f))
                    R-=(TMRL&0x1f);
                else
                    R=(TMRL&0x1f)-R;
            else
                R+=(TMRL&0x1f);
            
            for(i=0;i<=R;i++)
                    DelayMs((UINT16)10); //ranura de operacion de trama
            /*--------------------------------------*/
            LED2=1;                      
            i=0;
            while(i< N_TRIES)
            {                  
                MiApp_DiscardMessage();
               
                 
                if(FunTx()) 
                {                      
                    SendPacket=TRUE;                    
                    /*Velociada maxima:Prueba de tx hasta 500ms entre tx sin bloqueos;
                     *Maxima Latencia: 2500 ms, en 3s se bloquea; 
                     */
                    //Tiempo de guarda para el procesamiento y seteo y recibir todo el paquete                                                                                          
                    do{
                        do{
                            
                            MiApp_MessageAvailable();
                            
                            DelayMs(100);

                            exit0=MiWi_TickGetDiff(MiWi_TickGet(),tick1) < ((ONE_SECOND/32)*GAP_TX); 
                            
                           }while(~MiWiStateMachine.bits.RxHasUserData & exit0);
                        // Siempre esta recibiendo tramas de Beacon o broadcast   
                        if(MiWiStateMachine.bits.RxHasUserData) 
                        {   
                            if(FunRx(Norm_RX)) //incluye borrador de RXBUFFER
                            {
                                i=N_TRIES+1;
                                App.N=0;
                                RequestData=TRUE;
                                StateCom=Norm;                                
                                
                            }else
                                App.N++; // hubo un problema en la recepcion                                                        
                        } 
                        
                      }while((MiWi_TickGetDiff(MiWi_TickGet(),tick1)< ((ONE_SECOND/32)*GAP_TX))&& (i>N_TRIES));                                        
                }
                i++;                
                tick1=MiWi_TickGet();
                      
            }
                        
            /* >Llega a N_Tryes si no hubo comunicacion posible.
               >Si es exitosa i =4.                        */
            if (i==N_TRIES)
                switch (StateCom)
                {
                        case Norm: 
                            StateCom= Diff; // La proxima entra a default directo
                            
                            
                          break;
                        default:   
                            //Siendo Diferido por X veces reinicio comunicacion
                            devState= devWaitJoin;
                            StatusAsoc= FALSE;    
                            //Requiere iniciar ciclo nuevamente Norm -> Diff
                            StateCom= Norm;
                }
            LED2=0; 
          break;
       /* 
        default:
            
            devState=devWaitJoin;
            StatusAsoc=FALSE;
        */     
    }
    
    while( MiApp_TransceiverPowerState(POWER_STATE_SLEEP));  
            
    return(StatusAsoc && RequestData && SendPacket);
}
