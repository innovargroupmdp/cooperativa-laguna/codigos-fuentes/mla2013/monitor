/* 
 * File:   user.h
 * Author: cba
 *
 * Created on February 27, 2018, 11:51 AM
 */

#include "App.h"

typedef enum RTCCState_t_def
    {

        RTCC_FIX,
        RTCC_ON,       

    }RTCCState_t;

#define Lsample          10    

#define OffSetADC_0      0  //asegura medicion entre (1.6-0.2)v, menor de 1.9v hay error    
#define OffSetADCplus_0  500
#define OffSetADC_1      200  //asegura medicion entre (2.5-1,7)v, menor de 1.9v hay error
#define OffSetADCplus_1  0 
#define OffSetADC_2      700  //asegura medicion entre (3-2.5)v, menor de 1.9v hay error    
#define OffSetADCplus_2  0  //asegura medicion entre (3-2.5)v, menor de 1.9v hay error        
#define OffSetADC_3      0   //asegura medicion entre (1.6-0.2)v, menor de 1.9v hay error
#define OffSetADCplus_3  1100   //asegura medicion entre (1.6-0.2)v, menor de 1.9v hay error    
#define OffSetADC_D      0  //asegura medicion entre (1.6-0.2)v, menor de 1.9v hay error


    

/* ****************************************************************************
 * codigos App.Tipo                                                           */
    
#define nOP                 NULL 
#define OP_NORMAL           1 
#define OP_ASOCIACION       2
#define OP_TIME             3
#define OP_ASOCIACION_PAN   4
#define OP_CONTROL          5   
            
/******************************************************************************* 
  Sub codigos App.Tipo>=5;
* 00-09: De RED:Cambio de Slot, RFD, FFD.
* 10-19: De Monitoreo Interno: temperatura, humedad, scaneo, consumo, generacion solar, velociadad viento.
* 20-29: De Control Interno: reset POW, reset blank, cambio PAN, cambio de canal, cambio de banda, control potencia, T de sleep.
* 30-39: De Control Externo: Control IOs, USB, CAN, USART,Display.
* 40-49: De Control Datos: Escritura de Variable, Lectura de Variable.
* 50-59: De Control Programa: Control de flujo de programa(vector de punteros a funciones).
* 60-69:
* 70-79:
* 80-89:
* 90-99:
*******************************************************************************/    
    
#define Ctrl_NET    0
#define Ctrl_NET_SLOT       0
#define Ctrl_NET_CHG_PAN    1
#define Ctrl_NET_CHG_CHN    2
#define Ctrl_NET_CHG_BND    3
#define Ctrl_NET_CHG_PTX    4
        
#define Ctrl_VIEW   1
#define Ctrl_VIEW_TEMP  0
#define Ctrl_VIEW_SCN   1
#define Ctrl_VIEW_PWR   3
#define Ctrl_VIEW_GNS   4
#define Ctrl_VIEW_WND   5

#define Ctrl_IN     2
#define Ctrl_IN_POR     0
#define Ctrl_IN_RSTM    1
#define Ctrl_IN_T_SLP   2

#define Ctrl_OUT   3
#define Ctrl_OUT_IO    0
#define Ctrl_OUT_USB   1
#define Ctrl_OUT_CAN   2
#define Ctrl_OUT_SPI   3
#define Ctrl_OUT_I2C   4
#define Ctrl_OUT_USART 5
#define Ctrl_OUT_DSPLY 6

#define Ctrl_DAT    4
#define Ctrl_DAT_EEP   0
#define Ctrl_DAT_SST   1
#define Ctrl_DAT_FLH   2

#define Ctrl_PRG    5
#define Ctrl_PRG_PTR   0    



void Set_int_Sense(void);

void control_Sense(void);    
            
    /* USER_H */
void FunChekOSC(void);
void StckChck(void);
void FunClearGlobal(void);
void SensState(void);
void InitInts(void);
void set_int_RTCC(void);
void FunSleep_USER(void);
void FunAwake(void);
WORD Read_VBGVoltage(void);
BYTE FunADC(void);
//WORD ReadTmr3(void);
DATE_M_t   *FunReadRTCC(void);
DATE_CLK_t *FunReadRTCC_Ext(void); 
void FunAlarm(void);
void Fun_Rtcc_Fix(void);
BYTE BcdtoByte(BYTE);
WORD Fun_Count_Fix(WORD);
BOOL FunRTCC(RTCCState_t,BOOL,BOOL);



