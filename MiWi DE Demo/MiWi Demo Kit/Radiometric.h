/* 
 * File:   Radiometric.h
 * Author: cba
 *
 * Created on January 5, 2024, 11:00 AM
 */

#ifndef RADIOMETRICSENSOR_H
#define	RADIOMETRICSENSOR_H

#include "Sensor.h"

typedef struct Radiometric Radiometric;

struct Radiometric
{
  /*Standar API*/
  Sensor sensor;
  //Main Measures
  BOOL  pulse;
  /*MIMIA extention*/
  BOOL  past;
  const Tech *type_t;
  unsigned int id;
};

/********************************************************************
 * Instance properties
 *******************************************************************/
BOOL activated(Radiometric);
BOOL hasReading(Radiometric);
int  timestamp(Radiometric);
/********************************************************************
 * Instance methods
 ********************************************************************/
void start(void);
void stop(void);
void radiometric_init(Radiometric*, Sense*, unsigned int, Tech* );
/********************************************************************
 * Events
 ********************************************************************/
void onActivate(void);
void onError(void);
void onReading(void);

#endif	/* RADIOMETRICSENSOR_H */

