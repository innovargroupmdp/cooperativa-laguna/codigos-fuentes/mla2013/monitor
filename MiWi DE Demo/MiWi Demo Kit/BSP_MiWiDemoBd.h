/********************************************************************
* FileName:		BSP_MiWiCardDemoBd.h
* Dependencies:    
* Processor:	PIC18	
* Complier:     Microchip C18 v3.04 or higher	
* Company:		Microchip Technology, Inc.
*
* Copyright and Disclaimer Notice
*
* Copyright � 2007-2010 Microchip Technology Inc.  All rights reserved.
*
* Microchip licenses to you the right to use, modify, copy and distribute 
* Software only when embedded on a Microchip microcontroller or digital 
* signal controller and used with a Microchip radio frequency transceiver, 
* which are integrated into your product or third party product (pursuant 
* to the terms in the accompanying license agreement).   
*
* You should refer to the license agreement accompanying this Software for 
* additional information regarding your rights and obligations.
*
* SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY 
* KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY 
* WARRANTY OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A 
* PARTICULAR PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE 
* LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, 
* CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY 
* DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO 
* ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, 
* LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, 
* TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT 
* NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*
*********************************************************************
* File Description:
*
*  This file defines functions used for demo board hardware
*
* Change History:
*  Rev   Date         Author    Description
*  1.0   2/09/2011    ccs       Initial revision
********************************************************************/
#ifndef __BSP_MIWI_DEMO_BD_H_
    #define __BSP_MIWI_DEMO_BD_H_

// System Clock Frequency
#define CLOCK_FREQ          8000000

// There are three ways to use NVM to store data: External EPROM, Data EEPROM and 
// programming space, with following definitions:
//      #define USE_EXTERNAL_EEPROM
//      #define USE_DATA_EEPROM
//      #define USE_PROGRAMMING_SPACE  
// Each demo board has defined the method of using NVM, as
// required by Network Freezer feature.        
//#define USE_EXTERNAL_EEPROM

//#define SUPPORT_TWO_SPI

// Define EEPROM_SHARE_SPI if external EEPROM shares the SPI 
// bus with RF transceiver
//#define EEPROM_SHARE_SPI

  //*******************RFIF***********************
  #define PHY_IRQ0            INTCONbits.INT0IF
  #define PHY_IRQ0_En         INTCONbits.INT0IE 
  #define IRQ0_INT_PIN        PORTBbits.RB0       //INT_0
  #define IRQ0_INT_TRIS       TRISBbits.TRISB0    //INT_0
  #define Data_nCS            LATBbits.LATB6      //menoria
  #define Data_nCS_TRIS       TRISBbits.TRISB6    //menoria

  // MRF24J40 Pin / bits Definitions
  #define RFIF                INTCONbits.INT0IF
  #define RFIE                INTCONbits.INT0IE
  #define PHY_CS              LATCbits.LATC4
  #define PHY_CS_TRIS         TRISCbits.TRISC4
  #define RF_INT_PIN          PORTBbits.RB0
  #define RF_INT_TRIS         TRISBbits.TRISB0    //INT_0
  #define PHY_WAKE            LATCbits.LATC7
  #define PHY_WAKE_TRIS       TRISCbits.TRISC7
  #define PHY_RESETn          LATCbits.LATC6
  #define PHY_RESETn_TRIS     TRISCbits.TRISC6

  // APDS9930 Pin/ bits Definitions        
  #define APDS_IF             INTCONbits.INT0IF
  #define APDS_IE             INTCONbits.INT0IE        
  //#define APDS_INT_PIN        PORTBbits.RB0
  //#define APDS_INT_TRIS       TRISBbits.TRISB0    //INT_0

  //---------------Sensor HALL
  //#define Sens_Re_TRIS        TRISCbits.TRISC2 //RP13
  //#define Sens_Re             PORTCbits.RC2
  //#define Sens_En_TRIS        TRISAbits.TRISA6
  //#define Sens_En             LATAbits.LATA6
  //#define Sens_En_On()        Sens_En=1
  //#define Sens_En_Off()       Sens_En=0
  /*
  #define Sens_CONT_En        T1CONbits.TMR1ON
  #define Sens_CONT_En_On()        Sens_CONT_En=1
  #define Sens_CONT_En_Off()       Sens_CONT_En=0
  */

  // AN Pin Definitions
  #define AN0_TRIS            TRISAbits.TRISA0
  #define AN1_TRIS            TRISAbits.TRISA1
  #define AN2_TRIS            TRISAbits.TRISA2
  #define BAT_TRIS            AN0_TRIS
  #define AN_BAT              0 

  // SPI2 Pin Definitions

  #define SPI_SCK             LATBbits.LATB3
  #define SCK_TRIS            TRISBbits.TRISB3
  #define SPI_SDO             LATBbits.LATB2
  #define SDO_TRIS            TRISBbits.TRISB2
  #define SPI_SDI             PORTBbits.RB1
  #define SDI_TRIS            TRISBbits.TRISB1

  // I2C1 Pin Definitions

  #define I2C_SCL1            PORTBbits.RB4
  #define SCL1_TRIS           TRISBbits.TRISB4
  #define I2C_SDA1            LATBbits.LATB5
  #define SDA1_TRIS           TRISBbits.TRISB5

  // USART2 Pin Definitions

  #define USART_TX2           LATAbits.LATA1   //RP1
  #define TX2_TRIS            TRISAbits.TRISA1
  #define USART_RX2           PORTAbits.RA5    //RP2
  #define RX2_TRIS            TRISAbits.TRISA5

  // TIMERS
  #define TMRL                TMR0L     //fines de randon o numero de secuencia en la nueva mla2017 usa TMR1

  // Switch and LED Pin Definitions

  #define SW1_PORT            PORTCbits.RC2
  #define SW1_TRIS            TRISCbits.TRISC2
  //#define SW2_PORT            PORTCbits.RC5
  //#define SW2_TRIS            TRISCbits.TRISC5

  #define LED1                LATAbits.LATA2
  #define LED1_TRIS           TRISAbits.TRISA2
  #define LED2                LATAbits.LATA3
  #define LED2_TRIS           TRISAbits.TRISA3
  #define LED3                LATAbits.LATA7
  #define LED3_TRIS           TRISAbits.TRISA7
  #define LED4                LATAbits.LATA6
  #define LED4_TRIS           TRISAbits.TRISA6
  #define LED0                LATCbits.LATC5
  #define LED0_TRIS           TRISCbits.TRISC5

  // RTCC        

  #define RTCC_Out            LED4       // fines de debugger
  #define RTCC_Out_TRIS       LED4_TRIS       

#endif
