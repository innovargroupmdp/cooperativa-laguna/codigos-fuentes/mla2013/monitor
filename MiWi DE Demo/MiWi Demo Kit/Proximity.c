/* 
 * File:   Proximity.c
 * Author: cba
 *
 * Created on December 15, 2023, 9:57 AM
 */
#include "GenericTypeDefs.h"
#include "Proximity.h"

/********************************************************************
 * Drivers includes
 ********************************************************************/
#include "APDS/APDS9930.h"
/********************************************************************
 * Private definitions
 ********************************************************************/
void proximity_set(Proximity*);
void proximity_clr(Proximity*);
void past_set(Proximity*);
void past_clr(Proximity*);
/********************************************************************
 * Instance properties
 ********************************************************************/
BOOL activated(Proximity *this)
{
  Sensor *sensor = &this->sensor;
  return sensor->activated;
}

BOOL hasReading(Proximity *this)
{
  Sensor *sensor = &this->sensor;
  return sensor->hasReading;
}

unsigned int timestamp(Proximity *this)
{
  Sensor *sensor = &this->sensor;
  return sensor->timestamp;
}
/********************************************************************
 * Instance methods
 ********************************************************************/
//   Public
void start(Proximity *this)
{
  Sensor *sensor = &this->sensor;
  
  if (!enablePower())
  {
    sensor->error= TRUE;
  }
  sensor->error= FALSE;
  sensor->activated = TRUE;
}
void stop(Proximity *this)
{
  Sensor *sensor = &this->sensor;
  
  if (!disablePower())
  {
    sensor->error= TRUE;
  }
  sensor->error= FALSE;
  sensor->activated = FALSE;
}

void proximity_get(Proximity *this)
{  
  Sensor *sensor = &this->sensor;
  
  this->past=this->proximity;
  if (!readProximity((unsigned int) &this->proximity))
  {
    sensor->error= TRUE;
  }
  sensor->error= FALSE;
}
void proximity_init(Proximity *this, Sense *type_s,unsigned int id, Tech *type_t ){
  static const SensorVtbl vtbl={
    activated,
    hasReading,
    timestamp,
    start,
    stop,
    onActivate,
    onError,
    onReading
  };
  //Sensor_setInterface(&vtbl);
  Sensor *sensor = &this->sensor;
  
  sensor->vptr= &vtbl;
  sensor->type_s= type_s;
  sensor->activated= FALSE;
  sensor->hasReading= FALSE;
  sensor->timestamp= 0;
  sensor->activate= FALSE;
  sensor->error= FALSE;
  sensor->reading= FALSE;
  
  this->proximity= 0;
  this->past= 0;
  this->type_t= type_t;
  this->id= id;
}
//   Private

/********************************************************************
 * Events
 ********************************************************************/

  void onActivate(void){}
  void onError(void){}
  void onReading(void){}
 /* 
 */



